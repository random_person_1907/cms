<?php

    namespace Admin;

    use \Core\Application;
    use \Entities\AdminPage;

    /**
     * Router
     *
     * Admin router, which finds page in database
     *
     * @author Prokhorenko Vladyslav <random.person.1907@gmail.com>
     * @version 1.1
     */
    class Router extends \Core\Classes\Router {
        /**
         * search
         *
         * Search admin page in database
         *
         * @access public
         * @param array $routeParts Array of aliases for page searching
         * @return false if page hasn`t found or array with pages
         */
        public function search(array $routeParts)
        {
            $routeParts = empty($routeParts) ? ["main"] : $routeParts;

            $Pages = [];
            $isFinded = true;

            $i = 0;
            $parentId = null;
            foreach ($routeParts as $part) {
                $Page = Application::$entityManager
                            ->getRepository("\Entities\AdminPage")
                            ->findOneBy([
                                "alias" => $part,
                                "parent" => $parentId
                            ]);

                if (is_null($Page)) {
                    $isFinded = false;
                    break;
                } else {
                    if (isset($Pages[$i - 1])) {
                        $Pages[$i - 1]->addChild($Page);
                        $Page->setParent($Pages[$i - 1]);
                    }

                    $Pages[$i] = $Page;
                    $parentId = $Page->getId();

                    $i++;
                }
            }

            return ($isFinded) ? $Pages : false;
        }
    }
?>
