<?php
    namespace Admin;

    use \Core\Application;
    use \Modules\User;

    class AdminModel {

        public function __construct()
        {
            User::setRepository("\Entities\Admin");
        }

        public function isAuthorized()
        {
            if (
                isset($_SESSION["isLoggedIn"]) &&
                $_SESSION["isLoggedIn"] &&
                isset($_SESSION["adminId"])
            ) {
                $admin = User::getUserById(Application::encode($_SESSION["adminId"]));
                if (
                    $admin &&
                    User::checkIpAddress($admin->getIpAddresses())
                ) {
                    return $admin;
                }
            }

            return false;
        }

        public function login($login, $password)
        {
            if ($this->isAuthorized()) {
                return true;
            }

            $checkIp = false;
            $checkPass = false;
            if ($admin = User::getUserByLogin($login)) {
                if ($checkPass = User::checkPassword($admin->getPassword(), $password)) {
                    $checkIp = User::checkIpAddress($admin->getIpAddresses());
                }
            } else if ($admin = User::getUserByEmail($login)) {
                if ($checkPass = User::checkPassword($admin->getPassword(), $password)) {
                    $checkIp = User::checkIpAddress($admin->getIpAddresses());
                }
            }

            if ($admin && $checkIp && $checkPass) {
                $_SESSION["isLoggedIn"] = true;
                $_SESSION["adminId"] = $admin->getId();
                return true;
            } else if ($admin && $checkPass && !$checkIp) {
                $code = User::generateCode();
                $admin->setCode($code);

                Application::$entityManager->persist($admin);
                Application::$entityManager->flush();
                $this->sendMailWithCode($code, $admin->getEmail());

                Application::addMessage("Oops... Your ip-address has been changed. Confirm mail has been send to email.", "message");

                return false;
            } else {
                Application::addMessage("Login or password is invalid");
                return false;
            }
        }

        public function sendMailWithCode($code, $email)
        {
            $subject = 'Confirm login into account';

            $headers = "From: " . $email . "\r\n";
            $headers .= "Reply-To: ". $email . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $message = "<a href=\"" . Application::getProtocol() . "://{$_SERVER["SERVER_NAME"]}/modules?controllerName=Admin&action=check&code={$code}&email={$email}\">Click to confirm login</a>";

            mail(
                $email,
                $subject,
                $message,
                $headers
            );
        }

        public function checkAuthData()
        {
            if (
                isset(Application::$_POST["login"]) &&
                isset(Application::$_POST["password"])
            ) {
                return [
                    Application::$_POST["login"],
                    Application::$_POST["password"]
                ];
            } else {
                return [
                    false,
                    false
                ];
            }
        }

        public function checkCode()
        {
            if (
                isset(Application::$_GET["code"]) &&
                isset(Application::$_GET["email"])
            ) {
                if (User::checkCode(
                    Application::$_GET["code"],
                    Application::$_GET["email"]
                )) {
                    $this->addIpAddress(Application::$_GET["email"]);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        public function addIpAddress($email)
        {
            $admin = User::getUserByEmail($email);
            $ipAddress = new \Entities\AdminIpAddress;
            $ipAddress->setIpAddress($_SERVER["REMOTE_ADDR"]);
            $ipAddress->addAdmin($admin);
            $admin->addIpAddress($ipAddress);

            Application::$entityManager->persist($ipAddress);
            Application::$entityManager->persist($admin);
            Application::$entityManager->flush();
        }
    }
?>
