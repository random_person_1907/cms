$(document).ready(function() {
    $("#page__preloader").delay(1000).fadeOut(1500);

    $('[data-toggle="popover"]').each(function() {
        let title = $(this).find(".popover__title").html() || "",
            content = $(this).find(".popover__content").html() || "";

        $(this).popover({
            title : title,
            content : content,
            html : true,
            trigger : "click"
        });
    });
});
