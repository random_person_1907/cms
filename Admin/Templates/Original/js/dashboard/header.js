$(document).ready(function() {
    $(".search .search__submit").click(function() {
        $(this).parents(".search").addClass("active");
        $(this).parents(".search").find(".search__input").fadeIn("ease").addClass("active");
        $(this).parents(".search").find(".search__close").addClass("active");
    });

    $(".search .search__close").click(function() {
        $(this).parents(".search").removeClass("active");
        $(this).parents(".search").find(".search__input").removeClass("active").hide();
        $(this).parents(".search").find(".search__close").removeClass("active");
    });

    $(".search-btn").click(function() {
        $(".search-input").toggleClass("square");
        $(".search-btn").toggleClass("close");
    })
});
