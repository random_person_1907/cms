<div id="page__preloader">
    <svg width="200" height="200" viewBox="0 0 100 100">
      <polyline class="line-cornered stroke-still" points="0,0 100,0 100,100" stroke-width="10" fill="none"></polyline>
      <polyline class="line-cornered stroke-still" points="0,0 0,100 100,100" stroke-width="10" fill="none"></polyline>
      <polyline class="line-cornered stroke-animation" points="0,0 100,0 100,100" stroke-width="10" fill="none"></polyline>
      <polyline class="line-cornered stroke-animation" points="0,0 0,100 100,100" stroke-width="10" fill="none"></polyline>
    </svg>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 no-padding">
            @include('dashboard/header')
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12">
            <main>
                {!! $Page->getContent() !!}
            </main>
        </div>
    </div>
</div>
@include('dashboard/additional')
@include('dashboard/footer')
