<div id="circularMenu" class="circular-menu">

  <a class="floating-btn" onclick="document.getElementById('circularMenu').classList.toggle('active');">
    <i class="fa fa-plus"></i>
  </a>

  <menu class="items-wrapper">
    <a href="#" class="menu-item fa fa-twitter"></a>
    <a href="#" class="menu-item fa fa-google-plus"></a>
    <a href="#" class="menu-item fa fa-linkedin"></a>
    <a href="/admin/pages/create" class="menu-item"  title="{{ $Language->__("TEXT_DASHBOARD_CREATE_PAGE") }}">
        <img src="{{ $Page->getTheme()->getFilepath() }}img/icons/add_page.png" alt="Add page">
    </a>
  </menu>

</div>
