<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-4 no-padding">
                <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img class="d-block w-100" src="{{ $Page->getTheme()->getFilepath() }}/img/login/slide1.jpg" alt="First slide">
                      <div class="carousel-caption d-none d-md-block">
                        <h5>Lorem ipsum dolor.</h5>
                        <p>...</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="{{ $Page->getTheme()->getFilepath() }}/img/login/slide2.jpg" alt="Second slide">
                      <div class="carousel-caption d-none d-md-block">
                        <h5>Lorem ipsum dolor.</h5>
                        <p>...</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="{{ $Page->getTheme()->getFilepath() }}/img/login/slide3.jpg" alt="Third slide">
                      <div class="carousel-caption d-none d-md-block">
                        <h5>Lorem ipsum dolor.</h5>
                        <p>...</p>
                      </div>
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
            </div>
            <div class="col-8">
                <form class="form" method="POST" action="/modules/?controllerName=Admin&action=login">
                    <div class="logo">
                        <img src="#" alt="Logo">
                    </div>
                    <div class="text">
                        <h1>{{ $Language->__("TEXT_LOGIN_WELCOME_BACK") }}</h1>
                        <p>{{ $Language->__("TEXT_LOGIN_PLEASE_SIGN_IN") }}</p>
                    </div>
                    <div class="messages">
                        @foreach ($messages as $message)
                            <p class="message__text {{ $message["type"] }}">{{ $message["text"] }}</p>
                        @endforeach
                    </div>
                    <div class="form__fields">
                        <div class="row">
                            <div class="col-6 no-padding">
                                <div class="form__field">
                                    <label for="login">
                                        Login
                                    </label>
                                    <input type="text" id="login" name="login" placeholder="Email or login here...">
                                </div>
                            </div>
                            <div class="col-6 no-padding">
                                <div class="form__field">
                                    <label for="password">
                                        Password
                                    </label>
                                    <input type="password" id="password" name="password" placeholder="Password here...">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form__remember">
                        <div class="form__remember__checkbox">
                            <input type="checkbox" id="form__remember__checkbox" name="remember">
                            <label for="form__remember__checkbox">Keep me logged in</label>
                        </div>
                    </div>
                    <div class="form__buttons">
                        <div class="row justify-content-end">
                            <div class="col-3">
                                <div class="form__button form__button_recover">
                                    <a href="#">Recover password</a>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form__button form__button_login">
                                    <input type="submit" value="Login to Dashboard">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
