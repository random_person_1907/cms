<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-4 no-padding">
                <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img class="d-block w-100" src="{{ $Page->getTheme()->getFilepath() }}/img/login/slide1.jpg" alt="First slide">
                      <div class="carousel-caption d-none d-md-block">
                        <h5>Lorem ipsum dolor.</h5>
                        <p>...</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="{{ $Page->getTheme()->getFilepath() }}/img/login/slide2.jpg" alt="Second slide">
                      <div class="carousel-caption d-none d-md-block">
                        <h5>Lorem ipsum dolor.</h5>
                        <p>...</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="{{ $Page->getTheme()->getFilepath() }}/img/login/slide3.jpg" alt="Third slide">
                      <div class="carousel-caption d-none d-md-block">
                        <h5>Lorem ipsum dolor.</h5>
                        <p>...</p>
                      </div>
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
            </div>
            <div class="col-8">
                <form class="form" method="POST">
                    <div class="logo">
                        <img src="#" alt="Logo">
                    </div>
                    <div class="text">
                        <h1>Forgot your Password?</h1>
                        <p>Use the form below to recover it</p>
                    </div>
                    <div class="form__fields">
                        <div class="row">
                            <div class="col-11 no-padding">
                                <div class="form__field">
                                    <label for="email">
                                        Email
                                    </label>
                                    <input type="text" id="email" placeholder="Email here...">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form__buttons">
                        <div class="row justify-content-between">
                            <div class="col-4">
                                <div class="form__button form__button_recover">
                                    <a href="#">Sign in existing account</a>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form__button form__button_login">
                                    <a href="/admin/">Login to Dashboard</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
