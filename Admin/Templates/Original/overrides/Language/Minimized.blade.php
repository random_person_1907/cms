<div
    class="flag"
    style="background-image: url({{ $Page->getTheme()->getFilepath() }}img/dashboard/flags/{{ $currentLanguage }}.png)"
    type="button"
    data-toggle="popover"
    data-placement="bottom"
>
    <div class="popover__title">Settings</div>
    <div class="popover__content">
        <div class="popover__container d-flex">
            <div class="popover__block">
                @foreach ($listLanguages as $language)
                    <a href="/modules?controllerName=Language&action=changeLang&language={{ $language }}">{{ $language }}</a>
                @endforeach
            </div>
        </div>
    </div>
</div>
