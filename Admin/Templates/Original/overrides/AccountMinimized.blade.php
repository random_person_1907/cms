<div class="account d-flex">
    <div
        class="account__photo d-flex"
        data-toggle="popover"
        data-placement="bottom"
    >
        <img src="{{ $User->getAvatar() }}" alt="{{ $User->getName() }}">
        <div class="icon"></div>
        <div class="popover__title">
            <div class="account__popover_header d-flex">
                <img src="{{ $User->getAvatar() }}" alt="{{ $User->getName() }}">
                <div class="account__info">
                    <div class="account__info_name">
                        {{ $User->getName() }}
                    </div>
                    <div class="account__info_type">
                        {{ $User->getType()->getName() }}
                    </div>
                </div>
                <div class="account__logout">
                    <a href="/modules?controllerName=Admin&action=logout">{{ $Language->__("TEXT_DASHBOARD_LOGOUT") }}</a>
                </div>
            </div>
        </div>
        <div class="popover__content">
            <div class="popover__container">
                <div class="popover__block">
                    <div class="popover__block__header">
                        {{ $Language->__("TEXT_DASHBOARD_ACTIVITY") }}
                    </div>
                    <div class="popover__block__content">
                        <div class="popover__link d-flex justify-content-between">
                            <a href="#">{{ $Language->__("TEXT_DASHBOARD_RECOVER_CHAT") }}</a>
                            <span class="popover__counter popover__counter_blue">888</span>
                        </div>
                        <div class="popover__link">
                            <a href="#">{{ $Language->__("TEXT_DASHBOARD_RECOVER_PASSWORD") }}</a>
                        </div>
                    </div>
                </div>
                <div class="popover__block">
                    <div class="popover__block__header">
                        {{ $Language->__("TEXT_DASHBOARD_MY_ACCOUNT") }}
                    </div>
                    <div class="popover__block__content">
                        <div class="popover__link d-flex justify-content-between">
                            <a href="#">{{ $Language->__("TEXT_DASHBOARD_SETTINGS") }}</a>
                            <span class="popover__counter popover__counter_green">{{ $Language->__("TEXT_DASHBOARD_NEW") }}</span>
                        </div>
                        <div class="popover__link d-flex justify-content-between">
                            <a href="#">{{ $Language->__("TEXT_DASHBOARD_MESSAGES") }}</a>
                            <span class="popover__counter popover__counter_yellow">512</span>
                        </div>
                        <div class="popover__link">
                            <a href="#">{{ $Language->__("TEXT_DASHBOARD_LOGS") }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="account__info">
        <div class="account__info_name">
            {{ $User->getName() }}
        </div>
        <div class="account__info_type">
            {{ $User->getType()->getName() }}
        </div>
    </div>
</div>
