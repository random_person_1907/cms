<?php
    namespace Admin;

    use \Core\Application;
    use \Modules\Shortcode;
    use \Philo\Blade\Blade;

    /**
     * AdminView
     *
     * Admin view for page additional processing
     *
     * @author Prokhorenko Vladyslav <random.person.1907@gmail.com>
     * @version 1.0
     */

    class AdminView extends \Core\View {
        public function render($Page, $User)
        {
            include_once ROOT . $Page->getTheme()->getFilepath() . "/functions/functions.php";

            if (file_exists(ROOT . $Page->getTheme()->getFilepath() . "/functions/" . $Page->getAlias() . ".functions.php")) {
                include_once ROOT . $Page->getTheme()->getFilepath() . "/functions/" . $Page->getAlias() . ".functions.php";
            } else if (file_exists(ROOT . $Page->getTheme()->getFilepath() . "/functions/index.functions.php")) {
                include_once ROOT . $Page->getTheme()->getFilepath() . "/functions/index.functions.php";
            }

            $this->connectTemplateEngine([
                ROOT . "/Core/Templates",
                ROOT . $Page->getTheme()->getFilepath(),
                ROOT . $Page->getTheme()->getFilepath() . "/sublayouts"
            ]);

            $Page->view = $this;
            $Page->html = $this
                ->getTemplateEngine()
                ->view()
                ->make("base", [
                    "Page" => $Page,
                    "User" => $User,
                    "JavaScript" => $this->generateJs(),
                    "Styles" => $this->generateCss(),
                    "messages" => Application::getMessages(),
                    "Language" => Application::$Language
                ])
                ->render();

            $this->clearAll();

            $ShortcodeModule = new Shortcode;
            $ShortcodeModule->setEntityManager(Application::$entityManager);
            $ShortcodeModule->render($Page);

            foreach ($ShortcodeModule->getParseInEndShortcodes() as $module) {
                $ShortcodeModule->renderModule($module, $Page);
            }

            echo $Page->html;

            Application::clearMessages();
        }
    }
?>
