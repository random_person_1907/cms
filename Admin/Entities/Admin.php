<?php

    namespace Entities;
    /**
    * @Entity
    * @Table(name="Admins")
    */
    class Admin extends \MappedSuperclasses\User
    {
        /**
         * @ManyToMany(targetEntity="\Entities\AdminIpAddress", inversedBy="users")
         * @JoinTable(name="AdminsIpAddresses",
         *  joinColumns={@JoinColumn(name="adminId", referencedColumnName="id")},
         *  inverseJoinColumns={@JoinColumn(name="ipAddressId", referencedColumnName="id")}
         * )
         */
       protected $ipAddresses;

       /**
        * @ManyToOne(targetEntity="\Entities\AdminType", inversedBy="admins")
        * @JoinColumn(name="TypeId")
        */
       protected $type;
    }
