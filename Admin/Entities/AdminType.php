<?php

    namespace Entities;
    /**
    * @Entity
    * @Table(name="AdminTypes")
    */
    class AdminType extends \MappedSuperclasses\UserType
    {
        /**
         * @ManyToMany(targetEntity="\Entities\AdminPermission", mappedBy="types")
         */
        protected $permissions;

        /**
         * @OneToMany(targetEntity="\Entities\Admin", mappedBy="type")
         */
        protected $admins;

        /**
         * Add admin.
         *
         * @param \Entities\Admin $admin
         *
         * @return Type
         */
        public function addAdmin(\Entities\Admin $admin)
        {
            $this->admins[] = $admin;

            return $this;
        }

        /**
         * Remove admin.
         *
         * @param \Entities\Admin $admin
         *
         * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
         */
        public function removeAdmin(\Entities\Admin $admin)
        {
            return $this->admins->removeElement($admin);
        }

        /**
         * Get admins.
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getAdmins()
        {
            return $this->admins;
        }
    }
