<?php

    namespace Entities;
    /**
    * @Entity
    * @Table(name="AdminPermissions")
    */
    class AdminPermission extends \MappedSuperclasses\Permission
    {
        /**
         * @ManyToMany(targetEntity="\Entities\AdminType", mappedBy="permissions")
         */
        protected $types;
    }
