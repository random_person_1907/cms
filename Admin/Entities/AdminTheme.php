<?php

    namespace Entities;
    /**
    * @Entity
    * @Table(name="AdminThemes")
    */
    class AdminTheme extends \MappedSuperclasses\Theme
    {
        /**
        * @OneToMany(targetEntity="\Entities\AdminPage", mappedBy="pages")
        */
        private $pages;
    }
