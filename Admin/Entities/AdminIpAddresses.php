<?php

    namespace Entities;
    /**
    * @Entity
    * @Table(name="AdminIpAddresses")
    */
    class AdminIpAddress extends \MappedSuperclasses\IpAddress
    {
        /**
         * @ManyToMany(targetEntity="\Entities\Admin", mappedBy="ipAddresses")
         */
        protected $admins;

        /**
         * Add admin.
         *
         * @param \MappedSuperclasses\Admin $admin
         *
         * @return IpAddress
         */
        public function addAdmin(\Entities\Admin $admin)
        {
            $this->admins[] = $admin;

            return $this;
        }

        /**
         * Remove admin.
         *
         * @param \MappedSuperclasses\Admin $admin
         *
         * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
         */
        public function removeAdmin(\Entities\Admin $admin)
        {
            return $this->admins->removeElement($admin);
        }

        /**
         * Get admins.
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getAdmins()
        {
            return $this->admins;
        }
    }
