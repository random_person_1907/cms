<?php

    namespace Entities;
    /**
    * @Entity
    * @Table(name="AdminPages")
    */
    class AdminPage extends \MappedSuperclasses\Page
    {
        /**
        * @ManyToOne(targetEntity="\Entities\AdminTheme", inversedBy="theme")
        * @JoinColumn(name="AdminThemeId")
        */
        protected $theme;
    }
