<?php
    /**
     *
     * Bootstrap file for including srcipts, classes, etc,
     * which can be used to compose or process admin page
     */

    /**
     *
     * Admin router
     */
    require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . "Router.php";
    /**
     *
     * Admin view
     */
    require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . "/Views/Admin.php";
    /**
     *
     * User module
     */
     require_once ROOT . $_ENV["MODULE_FOLDER_PATH"] . "/User/Bootstrap.php";

    /**
     *
     * Admin entities
     */
    require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . $_ENV["ADMIN_ENTITIES_PATH"] . "Admin.php";
    require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . $_ENV["ADMIN_ENTITIES_PATH"] . "AdminIpAddresses.php";
    require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . $_ENV["ADMIN_ENTITIES_PATH"] . "AdminPage.php";
    require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . $_ENV["ADMIN_ENTITIES_PATH"] . "AdminPageType.php";
    require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . $_ENV["ADMIN_ENTITIES_PATH"] . "AdminPermission.php";
    require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . $_ENV["ADMIN_ENTITIES_PATH"] . "AdminTheme.php";
    require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . $_ENV["ADMIN_ENTITIES_PATH"] . "AdminType.php";


    /**
     *
     * Admin models
     */
    require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . "Models/AdminModel.php";
    \Modules\User::setEntityManager(\Core\Application::$entityManager);

    /**
     *
     * Additional files
     */
?>
