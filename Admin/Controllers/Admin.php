<?php

    namespace Core\Controllers;

    use \Core\Application;
    use \Admin\Router;
    use \Admin\AdminView;
    use \Admin\Models\AdminModel;
    use \Entities\AdminPage;
    use \Entities\AdminPageType;
    use \Entities\AdminAccessType;
    use \Entities\AdminHeader;
    use \Entities\AdminFooter;
    use \Entities\AdminBody;
    use \Entities\AdminContent;
    use \Entities\AdminMenu;
    use \Entities\AdminSidebar;
    use \Entities\AdminTheme;
    use \Philo\Blade\Blade;
    use \Modules\User;

    /**
     * Admin
     *
     * Main admin controller, which control page composing and modules processing
     *
     * @author Prokhorenko Vladyslav <random.person.1907@gmail.com>
     * @version 1.0
     */
    class Admin extends \Core\Abstracts\Controller implements \Core\Interfaces\Controller {
        /**
         * action
         *
         * Method for starting admin controller work
         *
         * @access public
         * @param array $params Parameters, which send from application
         * @return void
         */
        public function action(array $params) {
            require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . "Bootstrap.php";

            $router = new Router;
            $routeParts = $router->explodeUrl($params["REQUEST_URI"], $params["ROUTE"]["regexp"]);
            $Pages = $router->search($routeParts);

            if (!$Pages) {
                die("404");
            }

            $currentPage = end($Pages);

            $adminModel = new \Admin\AdminModel;
            $admin = $adminModel->isAuthorized();
            if (!$admin && $currentPage->getAlias() !== "login") {
                Application::setLastVisitedPage();
                header("Location: /admin/login");
                die;
            } else if ($admin && $currentPage->getAlias() == "login") {
                header("Location: /admin/");
                die;
            }

            Application::setLastVisitedPage();

            $View = new AdminView;
            $currentPage->html = $View->render($currentPage, $admin);
        }

        public function login() {
            require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . "Bootstrap.php";

            $adminModel = new \Admin\AdminModel;
            list($login, $password) = $adminModel->checkAuthData();

            if (!$login && !$password) {
                Application::addMessage("Login or password is empty");
                header(Application::getLastVisitedPage());
                die;
            } else {
                if ($adminModel->login($login, $password)) {
                    Application::redirect("/admin/");
                } else {
                    Application::redirect(Application::getLastVisitedPage());
                }
            }
        }

        public function logout()
        {
            session_destroy();
            Application::redirect("/admin/login/");
        }

        public function check()
        {
            require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . "Bootstrap.php";

            $adminModel = new \Admin\AdminModel;
            if (!$adminModel->checkCode()) {
                Application::addMessage("Confirmation code is invalid");
            } else {
                Application::addMessage("Confirmation code is valid. Now you can login into account", "message");
            }

            Application::redirect(Application::getLastVisitedPage());
        }
    }
?>
