<div class="account d-flex">
    <div
        class="account__photo d-flex"
        data-toggle="popover"
        data-placement="bottom"
    >
        <img src="<?php echo e($User->getAvatar()); ?>" alt="<?php echo e($User->getName()); ?>">
        <div class="icon"></div>
        <div class="popover__title">
            <div class="account__popover_header d-flex">
                <img src="<?php echo e($User->getAvatar()); ?>" alt="<?php echo e($User->getName()); ?>">
                <div class="account__info">
                    <div class="account__info_name">
                        <?php echo e($User->getName()); ?>

                    </div>
                    <div class="account__info_type">
                        <?php echo e($User->getType()->getName()); ?>

                    </div>
                </div>
                <div class="account__logout">
                    <a href="/modules?controllerName=Admin&action=logout"><?php echo e($Language->__("TEXT_DASHBOARD_LOGOUT")); ?></a>
                </div>
            </div>
        </div>
        <div class="popover__content">
            <div class="popover__container">
                <div class="popover__block">
                    <div class="popover__block__header">
                        <?php echo e($Language->__("TEXT_DASHBOARD_ACTIVITY")); ?>

                    </div>
                    <div class="popover__block__content">
                        <div class="popover__link d-flex justify-content-between">
                            <a href="#"><?php echo e($Language->__("TEXT_DASHBOARD_RECOVER_CHAT")); ?></a>
                            <span class="popover__counter popover__counter_blue">888</span>
                        </div>
                        <div class="popover__link">
                            <a href="#"><?php echo e($Language->__("TEXT_DASHBOARD_RECOVER_PASSWORD")); ?></a>
                        </div>
                    </div>
                </div>
                <div class="popover__block">
                    <div class="popover__block__header">
                        <?php echo e($Language->__("TEXT_DASHBOARD_MY_ACCOUNT")); ?>

                    </div>
                    <div class="popover__block__content">
                        <div class="popover__link d-flex justify-content-between">
                            <a href="#"><?php echo e($Language->__("TEXT_DASHBOARD_SETTINGS")); ?></a>
                            <span class="popover__counter popover__counter_green"><?php echo e($Language->__("TEXT_DASHBOARD_NEW")); ?></span>
                        </div>
                        <div class="popover__link d-flex justify-content-between">
                            <a href="#"><?php echo e($Language->__("TEXT_DASHBOARD_MESSAGES")); ?></a>
                            <span class="popover__counter popover__counter_yellow">512</span>
                        </div>
                        <div class="popover__link">
                            <a href="#"><?php echo e($Language->__("TEXT_DASHBOARD_LOGS")); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="account__info">
        <div class="account__info_name">
            <?php echo e($User->getName()); ?>

        </div>
        <div class="account__info_type">
            <?php echo e($User->getType()->getName()); ?>

        </div>
    </div>
</div>
<?php /**PATH /var/www/html/cms.loc/Admin/Templates/Original/shortcodes/AccountMinimized.blade.php ENDPATH**/ ?>