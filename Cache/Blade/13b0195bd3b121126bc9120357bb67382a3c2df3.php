<div id="page__preloader">
    <svg width="200" height="200" viewBox="0 0 100 100">
      <polyline class="line-cornered stroke-still" points="0,0 100,0 100,100" stroke-width="10" fill="none"></polyline>
      <polyline class="line-cornered stroke-still" points="0,0 0,100 100,100" stroke-width="10" fill="none"></polyline>
      <polyline class="line-cornered stroke-animation" points="0,0 100,0 100,100" stroke-width="10" fill="none"></polyline>
      <polyline class="line-cornered stroke-animation" points="0,0 0,100 100,100" stroke-width="10" fill="none"></polyline>
    </svg>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 no-padding">
            <?php echo $__env->make('dashboard/header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12">
            <main>
                <?php echo $Page->getContent(); ?>

            </main>
        </div>
    </div>
</div>
<?php echo $__env->make('dashboard/additional', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('dashboard/footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH /var/www/html/cms.loc/Admin/Templates/Original/sublayouts/dashboard/body.blade.php ENDPATH**/ ?>