<aside>
    <input id="menu-toggle-input" type="checkbox" />
    <label class="menu-toggle" for="menu-toggle-input">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </label>
    <nav class="menu">
    <ol>
      <li class="menu-item"><a href="/admin/"><?php echo e($Language->__("TEXT_DASHBOARD_HOME")); ?></a></li>
      <li class="menu-item"><a href="#0">About</a></li>
      <li class="menu-item">
        <a href="#0">Widgets</a>
        <ol class="sub-menu">
          <li class="menu-item"><a href="#0">Big Widgets</a></li>
          <li class="menu-item"><a href="#0">Bigger Widgets</a></li>
          <li class="menu-item"><a href="#0">Huge Widgets</a></li>
        </ol>
      </li>
      <li class="menu-item">
        <a href="#0">Kabobs</a>
        <ol class="sub-menu">
          <li class="menu-item"><a href="#0">Shishkabobs</a></li>
          <li class="menu-item"><a href="#0">BBQ kabobs</a></li>
          <li class="menu-item"><a href="#0">Summer kabobs</a></li>
          <li class="menu-item"><a href="#0">Shishkabobs</a></li>
          <li class="menu-item"><a href="#0">Shishkabobs</a></li>
          <li class="menu-item"><a href="#0">Shishkabobs</a></li>
          <li class="menu-item"><a href="#0">Shishkabobs</a></li>
          <li class="menu-item"><a href="#0">Shishkabobs</a></li>
          <li class="menu-item"><a href="#0">Shishkabobs</a></li>
          <li class="menu-item"><a href="#0">Shishkabobs</a></li>
          <li class="menu-item"><a href="#0">Shishkabobs</a></li>
        </ol>
      </li>
      <li class="menu-item"><a href="#0">Contact</a></li>
    </ol>
  </nav>
</aside>
<?php /**PATH /var/www/html/cms.loc/Admin/Templates/Original/sublayouts/dashboard/sidebar.blade.php ENDPATH**/ ?>