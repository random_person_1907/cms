<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo e($Page->getTitle()); ?></title>
    <script src="http://localhost:35729/livereload.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>

    <?php echo $JavaScript; ?>

    [[ Snippets.include:type="js" | parseInEnd ]]

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <?php echo $Styles; ?>

    [[ Snippets.include:type="css" | parseInEnd ]]
</head>
<body>
    <?php if(file_exists(ROOT . $Page->getTheme()->getFilepath() . "/" . $Page->getAlias() . ".blade.php")): ?>
        <?php echo $__env->make("{$Page->getAlias()}", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php else: ?>
        <?php echo $__env->make("index", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
</body>
</html>
<?php /**PATH /var/www/html/cms.loc/Core/Templates/base.blade.php ENDPATH**/ ?>