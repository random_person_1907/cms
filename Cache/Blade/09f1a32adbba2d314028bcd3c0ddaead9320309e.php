<div id="circularMenu" class="circular-menu">

  <a class="floating-btn" onclick="document.getElementById('circularMenu').classList.toggle('active');">
    <i class="fa fa-plus"></i>
  </a>

  <menu class="items-wrapper">
    <a href="#" class="menu-item fa fa-twitter"></a>
    <a href="#" class="menu-item fa fa-google-plus"></a>
    <a href="#" class="menu-item fa fa-linkedin"></a>
    <a href="/admin/pages/create" class="menu-item"  title="<?php echo e($Language->__("TEXT_DASHBOARD_CREATE_PAGE")); ?>">
        <img src="<?php echo e($Page->getTheme()->getFilepath()); ?>img/icons/add_page.png" alt="Add page">
    </a>
  </menu>

</div>
<?php /**PATH /var/www/html/cms.loc/Admin/Templates/Original/sublayouts/dashboard/additional.blade.php ENDPATH**/ ?>