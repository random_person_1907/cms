<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-3 d-flex">
                <?php echo $__env->make('dashboard/sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="site-tools d-flex">
                    <div class="site-tools__item">
                        <button
                            type="button"
                            class="btn btn-primary site-tools__item"
                            data-toggle="popover"
                            data-placement="bottom"
                        ><?php echo e($Language->__("TEXT_DASHBOARD_SITES")); ?></button>
                        <div class="popover__title">Sites</div>
                        <div class="popover__content">
                            <div class="popover__container d-flex">
                                <div class="popover__block">
                                    <a href="#">Shop</a>
                                </div>
                                <div class="popover__block">
                                    <a href="#">Blog</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="site-tools__item">
                        <button
                            type="button"
                            class="btn btn-primary site-tools__item"
                            data-toggle="popover"
                            data-placement="bottom"
                        ><?php echo e($Language->__("TEXT_DASHBOARD_PROJECTS")); ?></button>
                        <div class="popover__title">Settings</div>
                        <div class="popover__content">
                            <div class="popover__container d-flex">
                                <div class="popover__block">
                                    <a href="#">Shop</a>
                                </div>
                                <div class="popover__block">
                                    <a href="#">Blog</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="site-tools__item">
                        <button
                            type="button"
                            class="btn btn-primary site-tools__item"
                            data-toggle="popover"
                            data-placement="bottom"
                        ><?php echo e($Language->__("TEXT_DASHBOARD_SETTINGS")); ?></button>
                        <div class="popover__title">Settings</div>
                        <div class="popover__content">
                            <div class="popover__container d-flex">
                                <div class="popover__block">
                                    <a href="#">Shop</a>
                                </div>
                                <div class="popover__block">
                                    <a href="#">Blog</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="search">
                    <input type="text" placeholder="Print to search" class="search__input">
                </div>
            </div>
            <div class="col-5">
                <div class="row">
                    <div class="offset-2 col-4">
                        <div class="account-tools d-flex justify-content-end">
                            <div class="account-tools__item">
                                <a href="#">Not</a>
                            </div>
                            <div class="account-tools__item">
                                [[ Language.show:template="Minimized" ]]
                            </div>
                            <div class="account-tools__item">
                                <a href="#">Stat</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        [[ Account.showMinimized:UserId="<?php echo e($User->getId()); ?>" | UserClass="Admin" | Template="AccountMinimized" ]]
                    </div>
                    <div class="col-1">
                        <div class="calendar">
                            <div class="calendar__icon">
                                <svg class="sc-bdVaJa fUuvxv" fill="#ffffff" width="20px" height="20px" viewBox="0 0 1024 1024" rotate="0"><path d="M704 192v-64h-32v64h-320v-64h-32v64h-192v704h768v-704h-192zM864 864h-704v-480h704v480zM864 352h-704v-128h160v64h32v-64h320v64h32v-64h160v128z"></path></svg>
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="server-info">
                            <div class="server-info__button">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<?php /**PATH /var/www/html/cms.loc/Admin/Templates/Original/sublayouts/dashboard/header.blade.php ENDPATH**/ ?>