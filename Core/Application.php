<?php
    namespace Core;

    use \Core\Classes\Router;
    use \Core\Classes\Cache;
    use \Doctrine\ORM\EntityManager;
    use \Ayesh\PHP_Timer\Timer;

    /**
     * Application
     *
     * CMS Application
     *
     * @author Prokhorenko Vladyslav <random.person.1907@gmail.com>
     * @version 1.0
     */
    class Application {
        private static $ApplicationName;
        /**
         * Array with cleared GET-parameters
         *
         * @var array
         * @access public
         */
        public static $_GET;

        /**
         * Array with cleared POST-parameters
         *
         * @var array
         * @access public
         */
        public static $_POST;

        /**
         * Array with cleared REQUEST-parameters
         *
         * @var array
         * @access public
         */
        public static $_REQUEST;

        /**
         * Cleared request URI
         *
         * @var string
         * @access public
         */
        public static $_REQUEST_URI;

        /**
         * Doctrine entity manager
         *
         * @var Doctrine\ORM\EntityManager
         * @access public
         */
        public static $entityManager;

        /**
         * Object for cache manipulate
         *
         * @var \Core\Classes\Cache
         * @access public
         */
        public static $cache;


        /**
         * Array with errors, which has been found in the process of program
         *
         * @var array
         * @access public
         */
        private static $errors;

        /**
         *
         * Last visited page
         *
         * @var string
         * @access private
         */
        private static $LastVisitedPage;

        /**
         *
         * Language object
         *
         * @var object
         * @access public
         */
        public static $Language;

        /**
         * setEntityManager
         *
         * Sets doctrine entity manager
         *
         * @access public
         * @param Doctrine\ORM\EntityManager $entityManager Object of doctrine entity manager
         * @return void
         */
        public static function setEntityManager(EntityManager $entityManager)
        {
            self::$entityManager = $entityManager;
        }

        /**
         * runTimer
         *
         * Runs timer of application work
         *
         * @access public
         * @return void
         */
        public static function runTimer()
        {
            Timer::start("application");
        }

        /**
         * stopTimer
         *
         * Stops timer of application work. Outputs application work duration
         *
         * @access public
         * @return void
         */
        public static function stopTimer()
        {
            //dump(Timer::read("application", Timer::FORMAT_SECONDS));
            Timer::stop("application");
        }

        /**
         * run
         *
         * Runs application work
         *
         * @access public
         * @return void
         */
        public static function run($requestUri = "") {
            self::$_GET = self::clear($_GET);
            self::$_POST = self::clear($_POST);
            self::$_REQUEST = self::clear($_REQUEST);
            self::$_REQUEST_URI = empty($requestUri) ? self::clearUrl($_SERVER["REQUEST_URI"]) : self::clearUrl($requestUri);

            self::$cache = new Cache;
            self::$cache
                ->setIsEnable($_ENV["CACHE_ENABLED"])
                ->setLifetime($_ENV["CACHE_LIFETIME"])
                ->setPath($_ENV["CACHE_FULLPAGE_STORAGE"])
                ->check(self::$_REQUEST_URI);

            self::$Language = new \Modules\Language;
            self::$Language->load();

            $Router = new Router;
            $Router->setDefaultController($_ENV["DEFAULT_CONTROLLER_PATH"]);
            $Router->connect([
                ROOT . $_ENV["DEFAULT_BASIC_ROUTES_PATH"],
                ROOT . $_ENV["DEFAULT_ADDITIONAL_ROUTES_PATH"],
            ]);
            $routeData = $Router->check(self::$_REQUEST_URI);

            Application::setApplicationName($routeData["controllerName"]);
            $routeData["controllerName"] = "\Core\Controllers\\" . $routeData["controllerName"];

            if (class_exists($routeData["controllerName"]) &&
                method_exists($routeData["controllerName"], $routeData["controllerMethod"])
            ) {
                \Log::debug("Controller loaded", $routeData);

                $controller = new $routeData["controllerName"];
                $controller->{$routeData["controllerMethod"]}([
                    "REQUEST_URI" => Application::$_REQUEST_URI,
                    "ROUTE" => $routeData
                ]);
            } else {
                \Log::critical("Couldn`t load controller", $routeData);
            }
        }

        public static function setApplicationName($name)
        {
            self::$ApplicationName = strtoupper($name);
        }

        public static function getApplicationName()
        {
            return self::$ApplicationName;
        }

        /**
         * clear
         *
         * Clear each array element from spaces and encodes special symbols. Deletes empty elements
         *
         * @access public
         * @param array $params Array for clearing
         * @return array Cleaned array
         */
        public static function clear(array $params)
        {
            $newParams = [];

            foreach ($params as $key => &$value) {
                $key = trim($key);

                if (is_array($value) || is_object($value)) {
                    foreach ($value as &$val) {
                        $val = trim($val);
                        if (strlen($val) !== 0) {
                            $newParams[$key][] = $val;
                        }
                    }
                } else {
                    $value = trim($value);
                    if (strlen($value) !== 0) {
                        $newParams[$key] = $value;
                    }
                }
            }

            return $newParams;
        }

        public static function encode($string)
        {
            return htmlentities($string);
        }

        public static function clearUrl(string $url)
        {
            return trim(explode("?", $url)[0]);
        }

        public static function getProtocol()
        {
            $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
            return isset($_SERVER["HTTPS"]) ? 'https' : 'http';
        }

        public static function redirect($requestUri)
        {
            header("Location: {$requestUri}");
            die;
        }

        public static function addMessage($text, $type = "error")
        {
            $_SESSION["messages"][] = [
                "type" => $type,
                "text" => $text
            ];
        }

        public static function getMessages()
        {
            return isset($_SESSION["messages"]) ? $_SESSION["messages"] : [];
        }

        public static function clearMessages()
        {
            if (isset($_SESSION["messages"]))
                unset($_SESSION["messages"]);
        }

        public static function setLastVisitedPage()
        {
            self::$LastVisitedPage = self::$_REQUEST_URI . "?";
            foreach (self::$_GET as $key => $value) {
                self::$LastVisitedPage .= "{$key}={$value}&";
            }
            self::$LastVisitedPage = substr(self::$LastVisitedPage, 0, strlen(self::$LastVisitedPage) - 1);

            $_SESSION["LastVisitedPage"] = self::$LastVisitedPage;
        }

        public static function getLastVisitedPage()
        {
            return isset($_SESSION["LastVisitedPage"]) ? $_SESSION["LastVisitedPage"] : "/";
        }
    }
?>
