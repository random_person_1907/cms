<?php

	namespace Core\Interfaces;

	/**
	 *
	 * Interface for creating controllers
	 *
	 * @author Prokhorenko Vladyslav <random.person.1907@gmail.com>
	 * @version 1.0
	 */
	interface Controller {
		/**
		 * action
		 *
		 * Method for starting controller work
		 *
		 * @access public
		 * @param array $params Parameters, which was sended from application
		 * @return void
		 */
		public function action(array $params);
	}
?>
