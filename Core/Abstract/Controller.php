<?php

    namespace Core\Abstracts;

    /**
     * Controllers
     *
     * Abstract class for creating controllers
     *
     * @author Prokhorenko Vladyslav <random.person.1907@gmail.com>
     * @version 1.0
     */
    abstract class Controller {
        protected $action;
        protected $settings;
        protected $parameters;
        protected $model;
        protected $view;

        /**
         * checkRequestType
         *
         * Check type of request: Ajax or Simple
         *
         * @access private
         * @return string ajax/simple
         */
        private function checkRequestType()
        {

        }

        /**
         * returnHtml
         *
         * If request type is simple, controller must return html-code
         *
         * @access private
         * @param object $Page Page object, which must contain html property
         * @return void
         */
        private function returnHtml($Page)
        {

        }

        /**
         * returnJson
         *
         * If request type is ajax, controller must return json-code
         *
         * @access private
         * @param object $Page Page object, which must contain header, html property and type of block, which returns
         * @return void
         */
        private function returnJson()
        {

        }

        /**
         * addModules
         *
         * Transfer of control modules
         *
         * @access private
         * @param object $Page Page object processing
         * @param array $modules Modules, which will process page
         * @return object
         */
        private function addModules($Page, $modules = [])
        {

        }

        protected function loadEntity($entityNamespace)
        {
            $entityFilepath = ROOT . str_replace("\\", "/", $entityNamespace);

            if (substr($entityFilepath, -1) === "*") {
                $entityFilepath = substr($entityFilepath, 0, strlen($entityFilepath) - 1);

                foreach(\Libs\Files::getFiles($entityFilepath) as $entity) {
                    $this->load($entityFilepath . $entity);
                }
            } else {
                $entityFilepath .= ".php";
                $this->load($entityFilepath);
            }
        }

        private function load($filepath)
        {
            if (file_exists($filepath)) {
                require_once $filepath;
            }
        }

        protected function loadBootstrap($filepath)
        {
            $bootstrapFilepath = ROOT . $filepath . "Bootstrap.php";
            if (file_exists($bootstrapFilepath)) {
                require_once $bootstrapFilepath;
            }
        }

        public function setParameters($parameters)
        {
            $this->parameters = $parameters;
            return $this;
        }

        public function setModel($model)
        {
            $this->model = $model;
            return $this;
        }

        public function setSettings($settings)
        {
            $this->settings = $settings;
            return $this;
        }

        public function setView($view)
        {
            $this->view = $view;
            return $this;
        }

        protected function output()
        {
            return $this->view->render($this->parameters);
        }
    }
?>
