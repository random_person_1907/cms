<?php
    namespace Core\Abstracts;

    abstract class Module extends Controller {
        private $filepath;

        public function setFilepath($filepath)
        {
            $this->filepath = $filepath;
        }

        public function getFilepath()
        {
            return $this->filepath;
        }
    }
?>
