<?php
    namespace MappedSuperclasses;

    /** @MappedSuperclass */
    class Page {
        /**
        * @Id
        * @Column(type="integer")
        * @GeneratedValue
        */
        private $id;

        /**
        * @ManyToOne(targetEntity="\MappedSuperclasses\PageType", inversedBy="type")
        * @JoinColumn(name="TypeId")
        */
        protected $type;

        /**
        * @Column(length=255)
        */
        protected $title;

        /**
        * @Column(type="text")
        */
        protected $content;

        /**
        * @Column(length=255)
        */
        protected $name;

        /**
        * @Column(length=255)
        */
        protected $alias;

        /**
        * @OneToMany(targetEntity="\MappedSuperclasses\Page", mappedBy="parent")
        */
        protected $children;

        /**
        * @ManyToOne(targetEntity="\MappedSuperclasses\Page", inversedBy="children")
        * @JoinColumn(name="ParentId")
        */
        protected $parent;

        /**
        * @Column(type="boolean")
        */
        protected $published;
        /**
         * Constructor
         */
        public function __construct()
        {
            $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        }

        /**
         * Get id.
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set title.
         *
         * @param string $title
         *
         * @return Page
         */
        public function setTitle($title)
        {
            $this->title = $title;

            return $this;
        }

        /**
         * Get title.
         *
         * @return string
         */
        public function getTitle()
        {
            return $this->title;
        }

        /**
         * Set content.
         *
         * @param string $content
         *
         * @return Page
         */
        public function setContent($content)
        {
            $this->content = $content;

            return $this;
        }

        /**
         * Get content.
         *
         * @return string
         */
        public function getContent()
        {
            return $this->content;
        }

        /**
         * Set name.
         *
         * @param string $name
         *
         * @return Page
         */
        public function setName($name)
        {
            $this->name = $name;

            return $this;
        }

        /**
         * Get name.
         *
         * @return string
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * Set alias.
         *
         * @param string $alias
         *
         * @return Page
         */
        public function setAlias($alias)
        {
            $this->alias = $alias;

            return $this;
        }

        /**
         * Get alias.
         *
         * @return string
         */
        public function getAlias()
        {
            return $this->alias;
        }

        /**
         * Set published.
         *
         * @param bool $published
         *
         * @return Page
         */
        public function setPublished($published)
        {
            $this->published = $published;

            return $this;
        }

        /**
         * Get published.
         *
         * @return bool
         */
        public function getPublished()
        {
            return $this->published;
        }

        /**
         * Set type.
         *
         * @param \MappedSuperclasses\PageType|null $type
         *
         * @return Page
         */
        public function setType(\MappedSuperclasses\PageType $type = null)
        {
            $this->type = $type;

            return $this;
        }

        /**
         * Get type.
         *
         * @return \MappedSuperclasses\PageType|null
         */
        public function getType()
        {
            return $this->type;
        }

        /**
         * Add child.
         *
         * @param \MappedSuperclasses\Page $child
         *
         * @return Page
         */
        public function addChild(\MappedSuperclasses\Page $child)
        {
            $this->children[] = $child;

            return $this;
        }

        /**
         * Remove child.
         *
         * @param \MappedSuperclasses\Page $child
         *
         * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
         */
        public function removeChild(\MappedSuperclasses\Page $child)
        {
            return $this->children->removeElement($child);
        }

        /**
         * Get children.
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getChildren()
        {
            return $this->children;
        }

        /**
         * Set parent.
         *
         * @param \MappedSuperclasses\Page|null $parent
         *
         * @return Page
         */
        public function setParent(\MappedSuperclasses\Page $parent = null)
        {
            $this->parent = $parent;

            return $this;
        }

        /**
         * Get parent.
         *
         * @return \MappedSuperclasses\Page|null
         */
        public function getParent()
        {
            return $this->parent;
        }

        /**
         * Set theme.
         *
         * @param \MappedSuperclasses\Theme|null $theme
         *
         * @return Page
         */
        public function setTheme(\MappedSuperclasses\Theme $theme = null)
        {
            $this->theme = $theme;

            return $this;
        }

        /**
         * Get theme.
         *
         * @return \MappedSuperclasses\Theme|null
         */
        public function getTheme()
        {
            return $this->theme;
        }
    }
?>
