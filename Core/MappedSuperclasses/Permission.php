<?php
    namespace MappedSuperclasses;

    /** @MappedSuperclass */
    class Permission {
        /**
         * @Id
         * @Column(type="integer")
         * @GeneratedValue
         */
         private $id;

         /**
         * @Column(length=255)
         */
         protected $name;

         /**
              * Constructor
              */
             public function __construct()
             {
                 $this->types = new \Doctrine\Common\Collections\ArrayCollection();
             }

             /**
              * Set name.
              *
              * @param string $name
              *
              * @return Permission
              */
             public function setName($name)
             {
                 $this->name = $name;

                 return $this;
             }

             /**
              * Get name.
              *
              * @return string
              */
             public function getName()
             {
                 return $this->name;
             }

             /**
              * Add type.
              *
              * @param \MappedSuperclasses\UserType $type
              *
              * @return Permission
              */
             public function addType(\MappedSuperclasses\UserType $type)
             {
                 $this->types[] = $type;

                 return $this;
             }

             /**
              * Remove type.
              *
              * @param \MappedSuperclasses\UserType $type
              *
              * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
              */
             public function removeType(\MappedSuperclasses\UserType $type)
             {
                 return $this->types->removeElement($type);
             }

             /**
              * Get types.
              *
              * @return \Doctrine\Common\Collections\Collection
              */
             public function getTypes()
             {
                 return $this->types;
             }
    }
?>
