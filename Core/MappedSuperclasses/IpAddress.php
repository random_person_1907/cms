<?php

    namespace MappedSuperclasses;

    /** @MappedSuperclass */
    class IpAddress {
        /**
        * @Id
        * @Column(type="integer")
        * @GeneratedValue
        */
        private $id;

        /**
        * @Column(length=1000)
        */
        protected $ipAddress;

        /**
         * Constructor
         */
        public function __construct()
        {
            $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        }

        /**
         * Set ipAddress.
         *
         * @param string $ipAddress
         *
         * @return IpAddress
         */
        public function setIpAddress($ipAddress)
        {
            $this->ipAddress = $ipAddress;

            return $this;
        }

        /**
         * Get ipAddress.
         *
         * @return string
         */
        public function getIpAddress()
        {
            return $this->ipAddress;
        }
    }
?>
