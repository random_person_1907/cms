<?php

    namespace MappedSuperclasses;

    /** @MappedSuperclass */
    class Module {
        /**
        * @Id
        * @Column(type="integer")
        * @GeneratedValue
        */
        private $id;

        /**
        * @Column(length=1000)
        */
        private $name;

        /**
        * @Column(length=3000)
        */
        private $description;

        /**
        * @Column(length=1000)
        */
        private $controllerFilepath;

        /**
        * @Column(length=1000)
        */
        private $moduleFilepath;

        /**
        * @Column(type="datetime")
        */
        private $createdOn;

        /**
        * @Column(type="datetime")
        */
        private $modifiedOn;

        /**
        * @Column(type="boolean")
        */
        private $published;

        /**
        * @Column(length=10000, type="json")
        */
        private $settings;

        /**
             * Get id.
             *
             * @return int
             */
            public function getId()
            {
                return $this->id;
            }

            /**
             * Set name.
             *
             * @param string $name
             *
             * @return Module
             */
            public function setName($name)
            {
                $this->name = $name;

                return $this;
            }

            /**
             * Get name.
             *
             * @return string
             */
            public function getName()
            {
                return $this->name;
            }

            /**
             * Set description.
             *
             * @param string $description
             *
             * @return Module
             */
            public function setDescription($description)
            {
                $this->description = $description;

                return $this;
            }

            /**
             * Get description.
             *
             * @return string
             */
            public function getDescription()
            {
                return $this->description;
            }

            /**
             * Set controllerFilepath.
             *
             * @param string $controllerFilepath
             *
             * @return Module
             */
            public function setControllerFilepath($controllerFilepath)
            {
                $this->controllerFilepath = $controllerFilepath;

                return $this;
            }

            /**
             * Get controllerFilepath.
             *
             * @return string
             */
            public function getControllerFilepath()
            {
                $controllerFilepath = "return \"" . $this->controllerFilepath . "\";";
                $controllerFilepath = eval($controllerFilepath);
                return $controllerFilepath;
            }

            /**
             * Set moduleFilepath.
             *
             * @param string $moduleFilepath
             *
             * @return Module
             */
            public function setModuleFilepath($moduleFilepath)
            {
                $this->moduleFilepath = $moduleFilepath;

                return $this;
            }

            /**
             * Get moduleFilepath.
             *
             * @return string
             */
            public function getModuleFilepath()
            {
                $moduleFilepath = "return \"" . $this->moduleFilepath . "\";";
                $moduleFilepath = eval($moduleFilepath);
                return $moduleFilepath;
            }

            /**
             * Set createdOn.
             *
             * @param \DateTime $createdOn
             *
             * @return Module
             */
            public function setCreatedOn($createdOn)
            {
                $this->createdOn = $createdOn;

                return $this;
            }

            /**
             * Get createdOn.
             *
             * @return \DateTime
             */
            public function getCreatedOn()
            {
                return $this->createdOn;
            }

            /**
             * Set modifiedOn.
             *
             * @param \DateTime $modifiedOn
             *
             * @return Module
             */
            public function setModifiedOn($modifiedOn)
            {
                $this->modifiedOn = $modifiedOn;

                return $this;
            }

            /**
             * Get modifiedOn.
             *
             * @return \DateTime
             */
            public function getModifiedOn()
            {
                return $this->modifiedOn;
            }

            /**
             * Set published.
             *
             * @param bool $published
             *
             * @return Module
             */
            public function setPublished($published)
            {
                $this->published = $published;

                return $this;
            }

            /**
             * Get published.
             *
             * @return bool
             */
            public function getPublished()
            {
                return $this->published;
            }

            /**
             * Set settings.
             *
             * @param string $settings
             *
             * @return Module
             */
            public function setSettings($settings)
            {
                $this->settings = $settings;

                return $this;
            }

            /**
             * Get settings.
             *
             * @return string
             */
            public function getSettings()
            {
                return $this->settings;
            }
    }
