<?php

    namespace MappedSuperclasses;

    /** @MappedSuperclass */
    class UserType {
        /**
         * @Id
         * @Column(type="integer")
         * @GeneratedValue
         */
         private $id;

         /**
         * @Column(length=255)
         */
         protected $name;

         /**
          * Constructor
          */
         public function __construct()
         {
             $this->permissions = new \Doctrine\Common\Collections\ArrayCollection();
             $this->users = new \Doctrine\Common\Collections\ArrayCollection();
         }

         /**
          * Set name.
          *
          * @param string $name
          *
          * @return Type
          */
         public function setName($name)
         {
             $this->name = $name;

             return $this;
         }

         /**
          * Get name.
          *
          * @return string
          */
         public function getName()
         {
             return $this->name;
         }

         /**
          * Add permission.
          *
          * @param \MappedSuperclasses\Permission $permission
          *
          * @return Type
          */
         public function addPermission(\MappedSuperclasses\Permission $permission)
         {
             $this->permissions[] = $permission;

             return $this;
         }

         /**
          * Remove permission.
          *
          * @param \MappedSuperclasses\Permission $permission
          *
          * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
          */
         public function removePermission(\MappedSuperclasses\Permission $permission)
         {
             return $this->permissions->removeElement($permission);
         }

         /**
          * Get permissions.
          *
          * @return \Doctrine\Common\Collections\Collection
          */
         public function getPermissions()
         {
             return $this->permissions;
         }
    }
?>
