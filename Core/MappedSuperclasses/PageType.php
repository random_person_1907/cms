<?php

    namespace MappedSuperclasses;

    /** @MappedSuperclass */
    class PageType {
        /**
        * @Id
        * @Column(type="integer")
        * @GeneratedValue
        */
        private $id;

        /**
        * @Column(length=255)
        */
        protected $name;

        /**
        * @OneToMany(targetEntity="\MappedSuperclasses\Page", mappedBy="page")
        */
        protected $pages;
        /**
             * Constructor
             */
            public function __construct()
            {
                $this->pages = new \Doctrine\Common\Collections\ArrayCollection();
            }

            /**
             * Set name.
             *
             * @param string $name
             *
             * @return PageType
             */
            public function setName($name)
            {
                $this->name = $name;

                return $this;
            }

            /**
             * Get name.
             *
             * @return string
             */
            public function getName()
            {
                return $this->name;
            }

            /**
             * Add page.
             *
             * @param \MappedSuperclasses\Page $page
             *
             * @return PageType
             */
            public function addPage(\MappedSuperclasses\Page $page)
            {
                $this->pages[] = $page;

                return $this;
            }

            /**
             * Remove page.
             *
             * @param \MappedSuperclasses\Page $page
             *
             * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
             */
            public function removePage(\MappedSuperclasses\Page $page)
            {
                return $this->pages->removeElement($page);
            }

            /**
             * Get pages.
             *
             * @return \Doctrine\Common\Collections\Collection
             */
            public function getPages()
            {
                return $this->pages;
            }
    }
