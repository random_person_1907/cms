<?php
    namespace MappedSuperclasses;

    /** @MappedSuperclass */
    class Theme {
        /**
        * @Id
        * @Column(type="integer")
        * @GeneratedValue
        */
        private $id;

        /**
        * @Column(length=255)
        */
        protected $name;

        /**
        * @Column(length=10000)
        */
        protected $description;

        /**
        * @Column(length=1000)
        */
        protected $filepath;

        /**
         * Constructor
         */
        public function __construct()
        {
            $this->pages = new \Doctrine\Common\Collections\ArrayCollection();
        }

        /**
         * Set name.
         *
         * @param string $name
         *
         * @return Theme
         */
        public function setName($name)
        {
            $this->name = $name;

            return $this;
        }

        /**
         * Get name.
         *
         * @return string
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * Set description.
         *
         * @param string $description
         *
         * @return Theme
         */
        public function setDescription($description)
        {
            $this->description = $description;

            return $this;
        }

        /**
         * Get description.
         *
         * @return string
         */
        public function getDescription()
        {
            return $this->description;
        }

        /**
         * Set filepath.
         *
         * @param string $filepath
         *
         * @return Theme
         */
        public function setFilepath($filepath)
        {
            $this->filepath = $filepath;

            return $this;
        }

        /**
         * Get filepath.
         *
         * @return string
         */
        public function getFilepath()
        {
            return $this->filepath;
        }

        /**
         * Add page.
         *
         * @param \MappedSuperclasses\Page $page
         *
         * @return Theme
         */
        public function addPage(\MappedSuperclasses\Page $page)
        {
            $this->pages[] = $page;

            return $this;
        }

        /**
         * Remove page.
         *
         * @param \MappedSuperclasses\Page $page
         *
         * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
         */
        public function removePage(\MappedSuperclasses\Page $page)
        {
            return $this->pages->removeElement($page);
        }

        /**
         * Get pages.
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getPages()
        {
            return $this->pages;
        }
    }
?>
