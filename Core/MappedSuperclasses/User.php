<?php

    namespace MappedSuperclasses;

    /** @MappedSuperclass */
    class User {
        /**
         * @Id
         * @Column(type="integer")
         * @GeneratedValue
         */
         private $id;

         /**
         * @Column(length=255)
         */
         protected $name;

         /**
         * @Column(length=255)
         */
         protected $login;

         /**
         * @Column(length=1000)
         */
         protected $email;

         /**
         * @Column(length=1000)
         */
         protected $password;

         /**
         * @Column(length=1000)
         */
         protected $additionalEmail;

         /**
         * @Column(length=255)
         */
         protected $code;

         /**
         * @Column(length=1000)
         */
         protected $avatar;

         /**
         * @Column(type="boolean")
         */
         protected $isBanned = false;

         /**
         * @Column(type="datetime")
         */
         protected $createdOn;

         /**
         * @Column(type="datetime")
         */
         protected $modifiedOn;

         /**
         * @Column(length=255)
         */
         protected $skype;

         /**
         * @Column(length=255)
         */
         protected $telegram;

         /**
         * @Column(length=255)
         */
         protected $whatsApp;

         /**
         * @Column(length=255)
         */
         protected $viber;

         /**
         * @Column(length=1000)
         */
         protected $linkedIn;

         /**
         * @Column(length=20)
         */
         protected $phone;

         /**
         * Constructor
         */
        public function __construct()
        {
            $this->ipAddresses = new \Doctrine\Common\Collections\ArrayCollection();
        }

        /**
         * Get id.
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set name.
         *
         * @param string $name
         *
         * @return User
         */
        public function setName($name)
        {
            $this->name = $name;

            return $this;
        }

        /**
         * Get name.
         *
         * @return string
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * Set login.
         *
         * @param string $login
         *
         * @return User
         */
        public function setLogin($login)
        {
            $this->login = $login;

            return $this;
        }

        /**
         * Get login.
         *
         * @return string
         */
        public function getLogin()
        {
            return $this->login;
        }

        /**
         * Set email.
         *
         * @param string $email
         *
         * @return User
         */
        public function setEmail($email)
        {
            $this->email = $email;

            return $this;
        }

        /**
         * Get email.
         *
         * @return string
         */
        public function getEmail()
        {
            return $this->email;
        }

        /**
         * Set password.
         *
         * @param string $password
         *
         * @return User
         */
        public function setPassword($password)
        {
            $this->password = $password;

            return $this;
        }

        /**
         * Get password.
         *
         * @return string
         */
        public function getPassword()
        {
            return $this->password;
        }

        /**
         * Set additionalEmail.
         *
         * @param string $additionalEmail
         *
         * @return User
         */
        public function setAdditionalEmail($additionalEmail)
        {
            $this->additionalEmail = $additionalEmail;

            return $this;
        }

        /**
         * Get additionalEmail.
         *
         * @return string
         */
        public function getAdditionalEmail()
        {
            return $this->additionalEmail;
        }

        /**
         * Set code.
         *
         * @param string $code
         *
         * @return User
         */
        public function setCode($code)
        {
            $this->code = $code;

            return $this;
        }

        /**
         * Get code.
         *
         * @return string
         */
        public function getCode()
        {
            return $this->code;
        }

        /**
         * Set avatar.
         *
         * @param string $avatar
         *
         * @return User
         */
        public function setAvatar($avatar)
        {
            $this->avatar = $avatar;

            return $this;
        }

        /**
         * Get avatar.
         *
         * @return string
         */
        public function getAvatar()
        {
            return $this->avatar;
        }

        /**
         * Set isBanned.
         *
         * @param bool $isBanned
         *
         * @return User
         */
        public function setIsBanned($isBanned)
        {
            $this->isBanned = $isBanned;

            return $this;
        }

        /**
         * Get isBanned.
         *
         * @return bool
         */
        public function getIsBanned()
        {
            return $this->isBanned;
        }

        /**
         * Set createdOn.
         *
         * @param \DateTime $createdOn
         *
         * @return User
         */
        public function setCreatedOn($createdOn)
        {
            $this->createdOn = $createdOn;

            return $this;
        }

        /**
         * Get createdOn.
         *
         * @return \DateTime
         */
        public function getCreatedOn()
        {
            return $this->createdOn;
        }

        /**
         * Set modifiedOn.
         *
         * @param \DateTime $modifiedOn
         *
         * @return User
         */
        public function setModifiedOn($modifiedOn)
        {
            $this->modifiedOn = $modifiedOn;

            return $this;
        }

        /**
         * Get modifiedOn.
         *
         * @return \DateTime
         */
        public function getModifiedOn()
        {
            return $this->modifiedOn;
        }

        /**
         * Set skype.
         *
         * @param string $skype
         *
         * @return User
         */
        public function setSkype($skype)
        {
            $this->skype = $skype;

            return $this;
        }

        /**
         * Get skype.
         *
         * @return string
         */
        public function getSkype()
        {
            return $this->skype;
        }

        /**
         * Set telegram.
         *
         * @param string $telegram
         *
         * @return User
         */
        public function setTelegram($telegram)
        {
            $this->telegram = $telegram;

            return $this;
        }

        /**
         * Get telegram.
         *
         * @return string
         */
        public function getTelegram()
        {
            return $this->telegram;
        }

        /**
         * Set whatsApp.
         *
         * @param string $whatsApp
         *
         * @return User
         */
        public function setWhatsApp($whatsApp)
        {
            $this->whatsApp = $whatsApp;

            return $this;
        }

        /**
         * Get whatsApp.
         *
         * @return string
         */
        public function getWhatsApp()
        {
            return $this->whatsApp;
        }

        /**
         * Set viber.
         *
         * @param string $viber
         *
         * @return User
         */
        public function setViber($viber)
        {
            $this->viber = $viber;

            return $this;
        }

        /**
         * Get viber.
         *
         * @return string
         */
        public function getViber()
        {
            return $this->viber;
        }

        /**
         * Set linkedIn.
         *
         * @param string $linkedIn
         *
         * @return User
         */
        public function setLinkedIn($linkedIn)
        {
            $this->linkedIn = $linkedIn;

            return $this;
        }

        /**
         * Get linkedIn.
         *
         * @return string
         */
        public function getLinkedIn()
        {
            return $this->linkedIn;
        }

        /**
         * Set phone.
         *
         * @param string $phone
         *
         * @return User
         */
        public function setPhone($phone)
        {
            $this->phone = $phone;

            return $this;
        }

        /**
         * Get phone.
         *
         * @return string
         */
        public function getPhone()
        {
            return $this->phone;
        }

        /**
         * Set type.
         *
         * @param \MappedSuperclasses\UserType|null $type
         *
         * @return User
         */
        public function setType(\MappedSuperclasses\UserType $type = null)
        {
            $this->type = $type;

            return $this;
        }

        /**
         * Get type.
         *
         * @return \MappedSuperclasses\UserType|null
         */
        public function getType()
        {
            return $this->type;
        }

        /**
         * Add ipAddress.
         *
         * @param \MappedSuperclasses\IpAddress $ipAddress
         *
         * @return User
         */
        public function addIpAddress(\MappedSuperclasses\IpAddress $ipAddress)
        {
            $this->ipAddresses[] = $ipAddress;

            return $this;
        }

        /**
         * Remove ipAddress.
         *
         * @param \MappedSuperclasses\IpAddress $ipAddress
         *
         * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
         */
        public function removeIpAddress(\MappedSuperclasses\IpAddress $ipAddress)
        {
            return $this->ipAddresses->removeElement($ipAddress);
        }

        /**
         * Get ipAddresses.
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getIpAddresses()
        {
            return $this->ipAddresses;
        }
    }
?>
