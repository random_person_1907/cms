<?php
    namespace Core;

    use Philo\Blade\Blade;
    use Libs\Files;

    /**
     * View
     *
     * Core view class
     *
     * @author Prokhorenko Vladyslav <random.person.1907@gmail.com>
     * @version 1.0
     */
    abstract class View {
        /**
         * Additional Javascript`s file for page
         *
         * @var array
         * @access protected
         */
        protected $js = [];

        /**
         * Additional Css`s file for page
         *
         * @var array
         * @access protected
         */
        protected $css = [];

        /**
         * Blade template engine
         *
         * @var Blade
         * @access protected
         */
        protected $templateEngine;

        /**
         * getBaseTemplate
         *
         * Returns base template for next rendering
         *
         * @access protected
         * @return string HTML if base template has been found or false, if not
         */
        protected function getBaseTemplate()
        {
            return file_exists(ROOT . "/Core/Templates/Base.php") ? file_get_contents(ROOT . "/Core/Templates/Base.php") : false;
        }

        /**
         * connectTemplateEngine
         *
         * Connects and configures template engine for next rendering
         *
         * @access protected
         * @param string $viewsPath Path to folder with views
         * @return void
         */
        protected function connectTemplateEngine($viewsPath) {
            $cache = ROOT . "/Cache" . $_ENV["CACHE_BLADE"];

            $this->templateEngine = new Blade($viewsPath, $cache);
        }

        /**
         * getTemplateEngine
         *
         * Returns object of template engine
         *
         * @access protected
         * @return Blade
         */
        protected function getTemplateEngine()
        {
            return $this->templateEngine;
        }

        /**
         * AddJs
         *
         * Adds additional javascript file to array
         *
         * @access public
         * @param string $jsPath Path to javascript file
         * @return void
         */
        public function addJs($jsPath, $isRemote = false)
        {
            $allFiles = false;

            if (substr($jsPath, strlen($jsPath) - 1, strlen($jsPath)) === "*") {
                $allFiles = true;
                $jsPath = substr($jsPath, 0, strlen($jsPath) - 1);
            }

            if (file_exists(ROOT . $jsPath) && !$isRemote) {
                if ($allFiles) {
                    $files = Files::getFiles(ROOT . $jsPath);

                    foreach ($files as $file) {
                        if (Files::getMimetype($file) === "js") {
                            $this->js[] = $jsPath . $file;
                        }
                    }
                } else {
                    $this->js[] = $jsPath;
                }
            } else if ($isRemote) {
                $this->js[] = $jsPath;
            }
        }

        /**
         * AddCss
         *
         * Adds additional css file to array
         *
         * @access public
         * @param string $cssPath Path to javascript file
         * @return void
         */
        public function addCss($cssPath, $isRemote = false)
        {
            $allFiles = false;

            if (substr($cssPath, strlen($cssPath) - 1, strlen($cssPath)) === "*") {
                $allFiles = true;
                $cssPath = substr($cssPath, 0, strlen($cssPath) - 1);
            }

            if (file_exists(ROOT . $cssPath) && !$isRemote) {
                if ($allFiles) {
                    $files = Files::getFiles(ROOT . $cssPath);

                    foreach ($files as $file) {
                        if (Files::getMimetype($file) === "css") {
                            $this->css[] = $cssPath . $file;
                        }
                    }
                } else {
                    $this->css[] = $cssPath;
                }
            } else if ($isRemote){
                $this->css[] = $cssPath;
            }
        }

        /**
         * generateJs
         *
         * Generates html from array with javascript`s files
         *
         * @access public
         * @return string
         */
        public function generateJs()
        {
            $scripts = "";
            foreach ($this->js as $script) {
                $scripts .= "<script src=\"{$script}\"></script>\n";
            }

            return $scripts;
        }

        /**
         * generateCss
         *
         * Generates html from array with css`s files
         *
         * @access public
         * @return string
         */
        public function generateCss()
        {
            $styles = "";
            foreach ($this->css as $style) {
                $styles .= "<link rel=\"stylesheet\" href=\"{$style}\" />\n";
            }

            return $styles;
        }

        protected function clearAll()
        {
            $this->css = [];
            $this->js = [];
        }

        protected function clearCss()
        {
            $this->css = [];
        }

        protected function clearJs()
        {
            $this->js = [];
        }

        protected function checkOverride($themeFilepath, $module)
        {
            $overridePath = "{$themeFilepath}overrides/{$module->getName()}/";
            $moduleFilepath = "return \"" . $module->getModuleFilepath() . "\";";
            $moduleFilepath = eval($moduleFilepath);

            if (file_exists(ROOT . $overridePath)) {
                return $overridePath;
            } else if (file_exists(ROOT . $moduleFilepath . "Templates/")) {
                return $moduleFilepath . "Templates/";
            } else {
                return false;
            }
        }

        protected function compose($parameters)
        {
            if (
                !isset($parameters["TemplatePath"]) ||
                !isset($parameters["Template"]) ||
                !$parameters["TemplatePath"] ||
                !$parameters["Template"]
            ) {
                return false;
            }

            $this->connectTemplateEngine([
                ROOT . $parameters["TemplatePath"]
            ]);

            return $this
                ->getTemplateEngine()
                ->view()
                ->make($parameters["Template"], $parameters)
                ->render();
        }
    }
?>
