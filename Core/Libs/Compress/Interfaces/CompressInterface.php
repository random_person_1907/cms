<?php
    namespace Lib\Compress\Interface;

    interface Compress {
        private function compressHtml();
        private function compressCss();
        private function compressJs();
    }
?>
