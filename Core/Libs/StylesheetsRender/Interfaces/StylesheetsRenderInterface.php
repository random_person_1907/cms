<?php
    namespace \Lib\StylesheetsRender;

    interface StylesheetsRenderInterface {
        private function renderLess();
        private function renderScss();
        private function renderSass();
    }
?>
