<?php
    namespace Libs;

    class Files {
        public static function getFiles($path)
        {
            return file_exists($path) ? array_diff(scandir($path), array('.', '..')) : [];
        }

        public static function getMimetype($fileName)
        {
            $fileNamePaths = explode(".", basename($fileName));

            return $fileNamePaths[count($fileNamePaths) - 1];
        }
    }
?>
