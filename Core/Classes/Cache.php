<?php
    namespace Core\Classes;

    use Core\Application;
    use MAChitgarha\Component\Pusheh;

    /**
     * Cache
     *
     * Class for cache manipulate
     *
     * @author Prokhorenko Vladyslav <random.person.1907@gmail.com>
     * @version 1.0
     */

    class Cache {
        /**
         *
         * Parameter which show is cache disabled or enabled
         *
         * @var bool
         * @access private
         */
        private $enabled;

        /**
         *
         * The time during which the file is relevant (minutes)
         *
         * @var int
         * @access private
         */
        private $lifetime;

        /**
         *
         * Cache folder path
         *
         * @var string
         * @access private
         */
        private $path;

        /**
         * setIsEnable
         *
         * Set cache enabling or disabling
         *
         * @access private
         * @param bool $value True if cache must be enabled or False if cache must be disabled
         * @return Cache
         */
        public function setIsEnable($value)
        {
            $this->enabled = (bool)$value;

            return $this;
        }

        /**
         * setLifetime
         *
         * Sets cache lifetime
         *
         * @access public
         * @param int $time Time in minutes
         * @return Cache
         */
        public function setLifetime(int $time)
        {
            $this->lifetime = $time;

            return $this;
        }

        /**
         * setPath
         *
         * Sets cache path
         *
         * @access public
         * @param string $cachePath Cache folder path
         * @return Cache
         */
        public function setPath($cachePath)
        {
            if (file_exists(ROOT . "/Cache" . $cachePath)) {
                $this->path = ROOT . "/Cache" . $cachePath;
            } else {
                $this->enable = false;
            }

            return $this;
        }

        /**
         * check
         *
         * Cache existence checks, output it if exists and ends scripts
         *
         * @access public
         * @param string $route Current request URL
         */
        public function check($route)
        {
            if (!$this->enabled)
                return $this;

            $this->createCachePath($route);

             if (
                 $this->checkExistence() &&
                 $this->checkTime()
             ) {
                $this->output();
            }
        }

        /**
         * createCachePath
         *
         * Creates name of cache file from route
         *
         * @access private
         * @param string $route Request URL
         */
        private function createCachePath($route) {
            $this->cachePath = $this->path . "cache" . str_replace("/", "-", $route) . ".html";
        }

        /**
         * checkExistence
         *
         * Checks the existence of a cache file and returns it if it exists.
         *
         * @access private
         * @return string HTML, if cache exists, or false, if not
         */
        private function checkExistence()
        {
            return (file_exists($this->cachePath)) ? file_get_contents($this->cachePath) : false;
        }

        /**
         * checkTime
         *
         * Check cache relevance
         *
         * @access private
         * @return bool true, if cache is relevant, or false, if not
         */
        private function checkTime()
        {
            return ((time() - filemtime($this->cachePath)) < $this->lifetime * 60) ? true : false;
        }

        /**
         * output
         *
         * Outputs founded cache and ends application work
         *
         * @access private
         * @return void
         */
        private function output()
        {
            echo file_get_contents($this->cachePath);
            die;
        }

        /**
         * add
         *
         * Adds new cache
         *
         * @access public
         * @param string $content HTML, which will be put in cache file
         * @return bool True, if cache was added or false, if not
         */
        public function add($content)
        {
            $this->createCachePath(Application::$_REQUEST_URI);

            if (
                @file_put_contents($this->cachePath, $content) &&
                @chmod($this->cachePath, 0777)
            )  {
                return true;
            } else {
                return false;
            }
        }

        /**
         * clear
         *
         * Clears cache folder from cache files
         *
         * @access public
         * @return void
         */
        public function clear()
        {
            try {
                Pusheh::clearDir(ROOT . "/Cache/");
            } catch (\Exception $e) {
                Application::$errors[] = $e->getMessage();
            }
        }
    }
?>
