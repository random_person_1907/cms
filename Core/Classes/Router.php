<?php

    namespace Core\Classes;

    /**
     * Router
     *
     * Main router register routes and create regular expression
     *
     * @author Prokhorenko Vladyslav <random.person.1907@gmail.com>
     * @version 1.1
     */

     class Router {
       /**
        * Array with route, regular expression and controller`s path
        *
        * @var array
        * @access private
        */
        private $routes = array();

        /**
         * Default controller
         *
         * @var array
         * @access private
         */
        private $defaultController = array();

        /**
         * connect
         *
         * Include files with base and additional routes
         *
         * @access public
         * @param array $routesPaths Paths to file with basic and additional routes
         * @return void
         */
        public function connect($routesPaths)
        {
            foreach ($routesPaths as $routePath) {
                if (file_exists($routePath)) {
                    require_once $routePath;
                }
            }
        }

        /**
         * setDefaultRoute
         *
         * Set default controller
         *
         * @access public
         * @param string $controllerPath Path to controller, which processing route
         * @return void
         */
        public function setDefaultController($controllerName, $action = "action")
        {
            $this->defaulController = [
                "controllerName"    => $controllerName,
                "controllerMethod"  => $action
            ];
        }

        /**
         * register
         *
         * Adding route, regular expression and controller`s path,
         * which processin route, into array
         *
         * @access public
         * @param string $route Registered route
         * @param string $controllerPath Path to controller, which processing route
         * @return void
         */
        public function register($route, $controllerName, $action = "action")
        {
            $this->routes[] = [
                "route"             => $route,
                "regexp"            => $this->getRegexp($route),
                "controllerName"    => $controllerName,
                "controllerMethod"  => $action
            ];
        }

        /**
         * getRegexp
         *
         * Return regular expression for route
         *
         * @access private
         * @param string $route Route
         * @return string
         */
        public function getRegexp($route)
        {
            return str_replace(["*", "/", "?"], [".*", "\\/", "\?"], $route);
        }

        /**
         * check
         *
         * Checking passed to function route for equivalence
         * registered routes. Regular expression use for comparsion
         *
         * @access public
         * @param string $requestUri Route for comparsion
         * @return array Array with controller`s name and path
         */
        public function check($requestUri)
        {
            foreach ($this->routes as $route) {
                if (preg_match("/^{$route["regexp"]}[\/]?$/", $requestUri)) {
                    return [
                        "route"            => $route["route"],
                        "regexp"           => $route["regexp"],
                        "controllerName"   => $route["controllerName"],
                        "controllerMethod" => $route["controllerMethod"]
                    ];
                }
            }

            return $this->defaulController;
        }

        /**
         * explodeUrl
         *
         * Replace base route in request URL and explode it by slash
         *
         * @access public
         * @param string $url Request URL
         * @param string $route Base route
         * @return string Normalized URL
         */
        public function explodeUrl($url, $route)
        {
            $route = str_replace([".*", "\\/"], ["", "/"], $route);

            $url = explode("?", $url)[0];
            $url = str_replace($route, "", $url);
            $url = explode("/", $url);

            foreach ($url as $key => $value) {
                if (strlen(trim($value)) == 0) {
                    unset($url[$key]);
                }
            }

            return $url;
        }
    }
?>
