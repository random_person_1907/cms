<?php
    /**
     * Log
     *
     * Class, which write information about cms working
     * into selected file
     *
     * @author Prokhorenko Vladyslav <random.person.1907@gmail.com>
     * @version 1.1
     */

    class Log {
        /**
         * Variable, which show enabled logging or not
         *
         * @var bool
         * @access private
         */
        private static $isEnabled;

        /**
         * Path to logs directory
         *
         * @var string
         * @access private
         */
        private static $storage;

        /**
         * Path to directory with logs, which contain debug messages
         *
         * @var string
         * @access private
         */
        private static $debugStorage;

        /**
         * Path to directory with logs, which contain informational messages
         *
         * @var string
         * @access private
         */
        private static $infoStorage;

        /**
         * Path to directory with logs, which contain notices
         *
         * @var string
         * @access private
         */
        private static $noticeStorage;

        /**
         * Path to directory with logs, which contain warnings
         *
         * @var string
         * @access private
         */
        private static $warningStorage;

        /**
         * Path to directory with logs, which contain errors
         *
         * @var string
         * @access private
         */
        private static $errorStorage;

        /**
         * Path to directory with logs, which contain critical messages
         *
         * @var string
         * @access private
         */
        private static $criticalStorage;

        /**
         * Path to directory with logs, which contain alerts
         *
         * @var string
         * @access private
         */
        private static $alertStorage;

        /**
         * Path to directory with logs, which contain emergency messages
         *
         * @var string
         * @access private
         */
        private static $emergencyStorage;

        /**
         * Debug messages file handler
         *
         * @var resource
         * @access private
         */
        private static $debugHandler;

        /**
         * Informational messages file handler
         *
         * @var resource
         * @access private
         */
        private static $infoHandler;

        /**
         * Notification messages file handler
         *
         * @var resource
         * @access private
         */
        private static $noticeHandler;

        /**
         * Warning messages file handler
         *
         * @var resource
         * @access private
         */
        private static $warningHandler;

        /**
         * Error messages file handler
         *
         * @var resource
         * @access private
         */
        private static $errorHandler;

        /**
         * Critical messages file handler
         *
         * @var resource
         * @access private
         */
        private static $criticalHandler;

        /**
         * Alert messages file handler
         *
         * @var resource
         * @access private
         */
        private static $alertHandler;

        /**
         * Emergency messages file handler
         *
         * @var resource
         * @access private
         */
        private static $emergencyHandler;

        /**
         * setEnable
         *
         * On/Off logging
         *
         * @access public
         * @param bool $isEnabled A value that indicates logging is on or off
         * @return void
         */
        public static function setEnable($isEnabled)
        {
            self::$isEnabled = (bool)$isEnabled;
        }

        /**
         * setPath
         *
         * Sets path for every type of logs
         *
         * @access public
         * @param string $logStorage Path to directory for logs
         * @return void
         */
        public static function setPath($logStorage)
        {
            try {
                self::$storage = $logStorage;

                self::$debugStorage = self::createDir("DEBUG");
                self::$infoStorage = self::createDir("INFO");
                self::$noticeStorage = self::createDir("NOTICE");
                self::$warningStorage = self::createDir("WARNING");
                self::$errorStorage = self::createDir("ERROR");
                self::$criticalStorage = self::createDir("CRITICAL");
                self::$alertStorage = self::createDir("ALERT");
                self::$emergencyStorage = self::createDir("EMERGENCY");
            } catch (\OutOfRangeException $e) {
                self::$isEnabled = false;
            }
        }

        /**
         * createDir
         *
         * Creates directories for every type of logs
         *
         * @access private
         * @param string $logType Type of logs
         * @return string Path to created directory
         */
        private static function createDir($logType)
        {
            $dirPath = ROOT . self::$storage . ucfirst(strtolower($logType)) . "/";

            if (file_exists($dirPath)) {
                return $dirPath;
            } else if (!mkdir($dirPath, 0755, true)) {
                throw new \OutOfRangeException;
            } else {
                return $dirPath;
            }
        }

        /**
         * openFiles
         *
         * Calls method for opening files for all types of logs
         *
         * @access public
         * @return void
         */
        public static function openFiles()
        {
            if (self::$isEnabled) {
                try {
                    self::openFile("debug");
                    self::openFile("info");
                    self::openFile("notice");
                    self::openFile("warning");
                    self::openFile("error");
                    self::openFile("critical");
                    self::openFile("alert");
                    self::openFile("emergency");
                } catch (\Exception $e) {
                    self::$isEnabled = false;
                }
            }
        }

        /**
         * openFile
         *
         * Opens file for one logs type
         *
         * @access private
         * @param string $logType Type of logs
         * @return void
         */
        public static function openFile($logType)
        {
            $storageVariableName = $logType . "Storage";
            $handlerVariableName = $logType . "Handler";

            $filePath = self::$$storageVariableName . $logType . "." . date($_ENV["DATE_FORMAT"]) . ".log";

            if (file_exists($filePath)) {
                self::$$handlerVariableName = fopen($filePath, "a");
            } else if (file_exists(self::$$storageVariableName)) {
                self::$$handlerVariableName = fopen($filePath, "w");
            } else {
                throw new \Exception;
            }

            if (!self::$$handlerVariableName) {
                throw new \Exception;
            }
        }

        /**
         * debug
         *
         * Writes debug log in file
         *
         * @access public
         * @param string $message Logs message
         * @param array $data Additional data for writing into file
         * @return void
         */
        public static function debug(string $message, array $data = [])
        {
            if (self::$isEnabled && $_ENV["DEVELOPER_MODE"]) {
                $dateTime = date($_ENV["DATE_FORMAT"] . " " . $_ENV["TIME_FORMAT"]);
                $message = trim($message);

                $logRow = "Debug >>> {$dateTime}. \"{$message}\".";

                if (!empty($data)) {
                    $dataStr = str_replace(["  ", "\n", ", )"], ["", " ", ")"], var_export($data, true));
                    $logRow .= " Data: [{$dataStr}]";
                }

                $logRow .= "\n";

                @fwrite(self::$debugHandler, $logRow);
            }
        }

        /**
         * info
         *
         * Writes info log in file
         *
         * @access public
         * @param string $message Logs message
         * @param array $data Additional data for writing into file
         * @return void
         */
        public static function info(string $message, array $data = [])
        {
            if (self::$isEnabled) {
                $dateTime = date($_ENV["DATE_FORMAT"] . " " . $_ENV["TIME_FORMAT"]);
                $message = trim($message);

                $logRow = "Info >>> {$dateTime}. \"{$message}\".";

                if (!empty($data)) {
                    $dataStr = str_replace(["  ", "\n", ", )"], ["", " ", ")"], var_export($data, true));
                    $logRow .= " Data: [{$dataStr}]";
                }

                $logRow .= "\n";

                @fwrite(self::$infoHandler, $logRow);
            }
        }

        /**
         * notice
         *
         * Writes notice log in file
         *
         * @access public
         * @param string $message Logs message
         * @param array $data Additional data for writing into file
         * @return void
         */
        public static function notice(string $message, array $data = [])
        {
            if (self::$isEnabled) {
                $dateTime = date($_ENV["DATE_FORMAT"] . " " . $_ENV["TIME_FORMAT"]);
                $message = trim($message);

                $logRow = "Notice >>> {$dateTime}. \"{$message}\".";

                if (!empty($data)) {
                    $dataStr = str_replace(["  ", "\n", ", )"], ["", " ", ")"], var_export($data, true));
                    $logRow .= " Data: [{$dataStr}]";
                }

                $logRow .= "\n";

                @fwrite(self::$noticeHandler, $logRow);
            }
        }

        /**
         * warning
         *
         * Writes warning log in file
         *
         * @access public
         * @param string $message Logs message
         * @param array $data Additional data for writing into file
         * @return void
         */
        public static function warning(string $message, array $data = [])
        {
            if (self::$isEnabled) {
                $dateTime = date($_ENV["DATE_FORMAT"] . " " . $_ENV["TIME_FORMAT"]);
                $message = trim($message);

                $logRow = "Warning >>> {$dateTime}. \"{$message}\".";

                if (!empty($data)) {
                    $dataStr = str_replace(["  ", "\n", ", )"], ["", " ", ")"], var_export($data, true));
                    $logRow .= " Data: [{$dataStr}]";
                }

                $logRow .= "\n";

                @fwrite(self::$warningHandler, $logRow);
            }
        }

        /**
         * error
         *
         * Writes error log in file
         *
         * @access public
         * @param string $message Logs message
         * @param array $data Additional data for writing into file
         * @return void
         */
        public static function error(string $message, array $data = [])
        {
            if (self::$isEnabled) {
                $dateTime = date($_ENV["DATE_FORMAT"] . " " . $_ENV["TIME_FORMAT"]);
                $message = trim($message);

                $logRow = "Error >>> {$dateTime}. \"{$message}\".";

                if (!empty($data)) {
                    $dataStr = str_replace(["  ", "\n", ", )"], ["", " ", ")"], var_export($data, true));
                    $logRow .= " Data: [{$dataStr}]";
                }

                $logRow .= "\n";

                @fwrite(self::$errorHandler, $logRow);
            }
        }

        /**
         * critical
         *
         * Writes critical log in file
         *
         * @access public
         * @param string $message Logs message
         * @param array $data Additional data for writing into file
         * @return void
         */
        public static function critical(string $message, array $data = [])
        {
            if (self::$isEnabled) {
                $dateTime = date($_ENV["DATE_FORMAT"] . " " . $_ENV["TIME_FORMAT"]);
                $message = trim($message);

                $logRow = "Critical >>> {$dateTime}. \"{$message}\".";

                if (!empty($data)) {
                    $dataStr = str_replace(["  ", "\n", ", )"], ["", " ", ")"], var_export($data, true));
                    $logRow .= " Data: [{$dataStr}]";
                }

                $logRow .= "\n";

                @fwrite(self::$criticalHandler, $logRow);
            }
        }

        /**
         * alert
         *
         * Writes alert log in file
         *
         * @access public
         * @param string $message Logs message
         * @param array $data Additional data for writing into file
         * @return void
         */
        public static function alert(string $message, array $data = [])
        {
            if (self::$isEnabled) {
                $dateTime = date($_ENV["DATE_FORMAT"] . " " . $_ENV["TIME_FORMAT"]);
                $message = trim($message);

                $logRow = "Alert >>> {$dateTime}. \"{$message}\".";

                if (!empty($data)) {
                    $dataStr = str_replace(["  ", "\n", ", )"], ["", " ", ")"], var_export($data, true));
                    $logRow .= " Data: [{$dataStr}]";
                }

                $logRow .= "\n";

                @fwrite(self::$alertHandler, $logRow);
            }
        }

        /**
         * emergency
         *
         * Writes emergency log in file
         *
         * @access public
         * @param string $message Logs message
         * @param array $data Additional data for writing into file
         * @return void
         */
        public static function emergency(string $message, array $data = [])
        {
            if (self::$isEnabled) {
                $dateTime = date($_ENV["DATE_FORMAT"] . " " . $_ENV["TIME_FORMAT"]);
                $message = trim($message);

                $logRow = "Emergency >>> {$dateTime}. \"{$message}\".";

                if (!empty($data)) {
                    $dataStr = str_replace(["  ", "\n", ", )"], ["", " ", ")"], var_export($data, true));
                    $logRow .= " Data: [{$dataStr}]";
                }

                $logRow .= "\n";

                @fwrite(self::$emergencyHandler, $logRow);
            }
        }

        /**
         * close
         *
         * Closes all opened logs files
         *
         * @access public
         * @return void
         */
        public static function close()
        {
            @fclose(self::$debugHandler);
            @fclose(self::$infoHandler);
            @fclose(self::$noticeHandler);
            @fclose(self::$warningHandler);
            @fclose(self::$errorHandler);
            @fclose(self::$criticalHandler);
            @fclose(self::$alertHandler);
            @fclose(self::$emergencyHandler);
        }
    }
?>
