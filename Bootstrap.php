<?php
    ini_set("display_errors", 1);
    /**
     *
     * Includes composer`s packages
     */
    require_once __DIR__ . "/vendor/autoload.php";

    /**
     *
     * Includes core files
     */
    require_once __DIR__ . "/Core/Defines.php";
    require_once __DIR__ . "/Core/Interfaces/Controller.php";
    require_once __DIR__ . "/Core/Abstract/Controller.php";
    require_once __DIR__ . "/Core/Abstract/Module.php";
    require_once __DIR__ . "/Core/Classes/Router.php";
    require_once __DIR__ . "/Core/Classes/Log.php";
    require_once __DIR__ . "/Core/Classes/Cache.php";
    require_once __DIR__ . "/Core/Views/View.php";
    require_once __DIR__ . "/Core/Application.php";

    require_once __DIR__ . "/Core/MappedSuperclasses/IpAddress.php";
    require_once __DIR__ . "/Core/MappedSuperclasses/Module.php";
    require_once __DIR__ . "/Core/MappedSuperclasses/Page.php";
    require_once __DIR__ . "/Core/MappedSuperclasses/PageType.php";
    require_once __DIR__ . "/Core/MappedSuperclasses/Permission.php";
    require_once __DIR__ . "/Core/MappedSuperclasses/Theme.php";
    require_once __DIR__ . "/Core/MappedSuperclasses/User.php";
    require_once __DIR__ . "/Core/MappedSuperclasses/UserType.php";

    require_once __DIR__ . "/Core/Libs/Files/Bootstrap.php";
    require_once __DIR__ . "/Core/Libs/TablePrefix/Bootstrap.php";

    use Doctrine\ORM\Tools\Setup;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\Dotenv\Dotenv;
    use Whoops\Run;
    use Core\Application;
    use Core\Classes\Router;

    /**
     *
     * Starts session
     */
    session_start();

    /**
     *
     * Creates application, runs timer for time accounting
     * Registers function to stop timer, when application will finish work
     */
    $Application = new Application;
    Application::runTimer();
    register_shutdown_function([$Application, 'stopTimer']);

    /**
     *
     * Registers error handler
     */
    $whoops = new Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();

    /**
     *
     * Parses configuration files
     */
    $dotenv = new Dotenv();
    $dotenv->load(
        ROOT . '/Configuration/.env',
        ROOT . '/Configuration/.env.dev',
        ROOT . '/Configuration/.env.cache',
        ROOT . '/Configuration/.env.log',
        ROOT . '/Configuration/.env.mail',
        ROOT . '/Configuration/.env.database'
    );

    /**
     *
     * Includes main controllers
     */
     require_once ROOT . $_ENV["COMMON_FOLDER_PATH"] . $_ENV["COMMON_CONTROLLER_PATH"];
     require_once ROOT . $_ENV["ADMIN_FOLDER_PATH"] . $_ENV["ADMIN_CONTROLLER_PATH"];
     require_once ROOT . $_ENV["MODULE_FOLDER_PATH"] . $_ENV["MODULE_CONTROLLER_PATH"];
     require_once ROOT . $_ENV["CRM_FOLDER_PATH"] . $_ENV["CRM_CONTROLLER_PATH"];

     /**
      *
      * Modules
      */
      require_once ROOT . $_ENV["MODULE_FOLDER_PATH"] . "Language/Bootstrap.php";
      require_once ROOT . $_ENV["MODULE_FOLDER_PATH"] . "User/Bootstrap.php";
      require_once ROOT . $_ENV["MODULE_FOLDER_PATH"] . "Shortcode/Bootstrap.php";


    /**
     *
     * Include modules entity
     */
    require_once ROOT . $_ENV["MODULE_FOLDER_PATH"] . $_ENV["MODULE_ENTITIES_PATH"] . "Module.php";

    /**
     *
     * Sets initial settings
     */
    ini_set("display_errors", $_ENV["DEVELOPER_MODE"]);
    ini_set("max_execution_time", $_ENV["MAX_EXECUTION_TIME"]);

    /**
     *
     * If logging is enable, log files opens. Registers function for close it,
     * when application will finish work
     */
    $Log = new \Log;
    \Log::setEnable($_ENV["LOG_ENABLE"]);
    \Log::setPath($_ENV["LOG_STORAGE"]);
    \Log::openFiles();
    register_shutdown_function([$Log, 'close']);

    /**
     *
     * Sets entities path for doctrine work
     */
    $entitiesPaths = [
        __DIR__ . "/Core/MappedSuperclasses/",
        __DIR__ . $_ENV["COMMON_FOLDER_PATH"] . $_ENV["COMMON_ENTITIES_PATH"],
        __DIR__ . $_ENV["ADMIN_FOLDER_PATH"] . $_ENV["ADMIN_ENTITIES_PATH"],
        __DIR__ . $_ENV["CRM_FOLDER_PATH"] . $_ENV["CRM_ENTITIES_PATH"],
        __DIR__ . $_ENV["MODULE_FOLDER_PATH"] . $_ENV["MODULE_ENTITIES_PATH"]
    ];

    /**
     *
     * The database connection configuration
     */
    $dbParams = array(
        'driver'   => $_ENV["DB_DRIVER"],
        'user'     => $_ENV["DB_USER"],
        'password' => $_ENV["DB_PASSWORD"],
        "host"     => $_ENV["DB_HOST"],
        'dbname'   => $_ENV["DB_NAME"],
    );


    /**
     *
     * Connects to database, checks errors and adds entity manager
     * to parameters of application class
     */
    $config = Setup::createAnnotationMetadataConfiguration($entitiesPaths, $_ENV["DEVELOPER_MODE"]);

    // Table Prefix
    $evm = new \Doctrine\Common\EventManager;
    $tablePrefix = new \DoctrineExtensions\TablePrefix('prefix_');
    $evm->addEventListener(\Doctrine\ORM\Events::loadClassMetadata, $tablePrefix);

    $entityManager = EntityManager::create($dbParams, $config, $evm);

    try {
        $entityManager->getConnection()->connect();
        $_ENV["DB_CONNECTION"] = true;
    } catch (\Exception $e) {
        $_ENV["DB_CONNECTION"] = false;
        \Log::error("Couldn`t connect to database");
    }

    Application::setEntityManager($entityManager);
?>
