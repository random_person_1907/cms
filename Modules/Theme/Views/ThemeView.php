<?php
    namespace Modules;

    class ThemeView extends \Core\View {
        public function render($parameters)
        {
            // header('Access-Control-Allow-Origin: true');
            $templatePath = $this->checkOverride(
                $parameters["Page"]->getTheme()->getFilepath(),
                $parameters["Module"]
            );

            if (!$templatePath) {
                return false;
            }

            $parameters["Page"]->view->addJs($templatePath . "js/*");
            $parameters["Page"]->view->addCss($templatePath . "css/*");

            $parameters["TemplatePath"] = $templatePath;

            return $this->compose($parameters);
        }
    }
?>
