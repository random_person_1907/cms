<?php
    namespace Modules;

    use \Core\Abstracts\Module;
    use \Core\Application;

    class Theme extends Module {
        private $template;

        public function include($parameters)
        {
            $this->loadBootstrap($parameters["Module"]->getModuleFilepath());
            $themeModel = new ThemeModel;
            $themeView = new ThemeView;
            $this
                ->setView($themeView)
                ->setModel($themeModel)
                ->setSettings($themeModel->getSettings())
                ->setParameters($parameters);

            return $this
                        ->getTemplate()
                        ->{"show" . $this->template}()
                        ->output();
        }

        private function getTemplate()
        {
            if (!isset($this->parameters["Template"])) {
                $this->template = "Default";
            } else {
                $this->template = $this->parameters["Template"];
            }

            return $this;
        }

        private function showDefault()
        {
            dump($this);die;
            return $this;
        }

        public function showInstall()
        {
            return $this;
        }

        public function showEdit()
        {
            dump($this);die;

            return $this;
        }

        private function showList()
        {
            return $this;
        }

        public function process($parameters)
        {
            $this->loadBootstrap($parameters["Module"]->getModuleFilepath());
            $themeModel = new ThemeModel;
            $themeView = new ThemeView;
            $this
                ->setView($themeView)
                ->setModel($themeModel)
                ->setSettings($themeModel->getSettings())
                ->setParameters($parameters);

            return $this
                        ->getAction()
                        ->{$this->action}()
                        ->output();
        }

        private function getAction()
        {
            if (!isset(Application::$_GET['type'])) {
                $this->action = "Default";
            } else {
                $this->action = Application::$_GET['type'];
            }

            return $this;
        }

        private function install()
        {
            $this->model->addTheme();
            dump($this);die;
        }
    }
?>
