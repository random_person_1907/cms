<div class="row justify-content-center h-100">
    <div class="col-10 h-100 mt-3">
        <h1>{{ $Page->getTitle() }}</h1>
        <div class="block white h-75 mt-5">
            <form
                class="w-100 h-100"
                action="/modules?controllerName=Theme&action=process&type=install"
                method="POST"
            >
                <div class="row justify-content-center h-100 w-100 m-0">
                    <div class="col-6 border-right no-padding">
                        <h3 class="header p-3">Информация о теме</h3>
                    </div>
                    <div class="col-6 h-100 text-center">
                        <label for="themeInput" class="w-100 h-85 pt-5 label">
                            <img src="https://img.icons8.com/officel/80/000000/upload-2.png" class="mt-5 pt-5">
                            <p class="mt-3">
                                Drag and drop to upload
                            </p>
                        </label>
                        <input id="themeInput" type="file" name="theme" class="input d-none">
                        <input type="submit" name="uploadTheme" class="button w-50" value="Install">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
