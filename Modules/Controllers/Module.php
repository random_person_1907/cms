<?php

	namespace Core\Controllers;

	use \Core\Application;
	use \Core\Models\ModuleModel;

	class Module  extends \Core\Abstracts\Module implements \Core\Interfaces\Controller {
		public function action(array $params)
		{
			$this->loadBootstrap($_ENV["MODULE_FOLDER_PATH"]);

			if (
				isset(Application::$_GET["controllerName"]) &&
				isset(Application::$_GET["action"]) &&
				class_exists("\Core\Controllers\\" . Application::$_GET["controllerName"])
			) {
				$controllerName = "\Core\Controllers\\" . Application::$_GET["controllerName"];
				$controller = new $controllerName;

				if (method_exists($controller, Application::$_GET["action"])) {
					$controller->{Application::$_GET['action']}([
						"Action" => Application::$_GET['action']
					]);
				}
			} else if (
				isset(Application::$_GET["controllerName"])	 &&
				isset(Application::$_GET["action"])
			) {
				$moduleModel = new ModuleModel;
				$module = $moduleModel->findModuleByName(Application::$_GET["controllerName"]);

				if ($module) {
					$moduleFilepath = "return \"" . $module->getModuleFilepath() . "\";";
					$moduleFilepath = eval($moduleFilepath);
					require_once ROOT . $moduleFilepath . $module->getControllerFilepath();

					$controllerName = "\Modules\\" . Application::$_GET["controllerName"];
					$controller = new $controllerName;

					if (method_exists($controller, Application::$_GET["action"])) {
						$controller->{Application::$_GET['action']}([
							"Module" => $module,
							"Action" => Application::$_GET['action']
						]);
					}
				}
			}
        }

        public function addModules(\Page $Page)
		{

        }
	}
?>
