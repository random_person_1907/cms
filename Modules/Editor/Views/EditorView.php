<?php
    namespace Modules;

    class EditorView extends \Core\View {
        public function render($parameters)
        {
            header('Access-Control-Allow-Origin: true');
            $templatePath = $this->checkOverride(
                $parameters["Page"]->getTheme()->getFilepath(),
                $parameters["Module"]
            );

            if (!$templatePath) {
                return false;
            }

            $parameters["Page"]->view->addJs("//cdn.quilljs.com/1.3.6/quill.min.js", true);
            $parameters["Page"]->view->addJs("https://momentjs.com/downloads/moment-with-locales.js", true);
            $parameters["Page"]->view->addJs("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js", true);
            $parameters["Page"]->view->addJs($templatePath . "js/*");

            $parameters["Page"]->view->addCss("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css", true);
            $parameters["Page"]->view->addCss("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css", true);
            $parameters["Page"]->view->addCss("//cdn.quilljs.com/1.3.6/quill.snow.css", true);
            $parameters["Page"]->view->addCss($templatePath . "css/*");
            $parameters["TemplatePath"] = $templatePath;

            return $this->compose($parameters);
        }
    }
?>
