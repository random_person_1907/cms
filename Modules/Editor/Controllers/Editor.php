<?php
    namespace Modules;

    use \Core\Abstracts\Module;

    class Editor extends Module {
        public function include($parameters)
        {
            $editorView = new EditorView;
            return $editorView->render($parameters);
        }
    }
?>
