<?php
    namespace Modules;

    use \Core\Application;

    class EditorModel {
        public function getSettings() {
            $module = Application::$entityManager
                        ->getRepository("\Entities\Module")
                        ->findOneBy([
                "name" => "Editor"
            ]);

            return ($module) ? $module->getSettings() : [];
        }
    }
?>
