$(document).ready(function() {
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],

        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction

        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],

        ['clean']                                         // remove formatting button
    ];

    var mainEditor = new Quill('.main-editor', {
        modules: {
          toolbar: toolbarOptions
        },
        theme: 'snow'
    });

    var additionalEditor = new Quill('.additional-editor', {
        modules: {
          toolbar: toolbarOptions
        },
        theme: 'snow'
    });

    $(".tab__header a").click(function(event) {
        event.preventDefault();
        let tabTarget = $(this).attr("tab-target") || false;

        if (tabTarget) {
            $(".tab__header a").removeClass("active");
            $(this).addClass("active");
            $(".tab__content").removeClass("active").hide();
            $(tabTarget).addClass("active").fadeIn("slow");
        }
    });

    $(".tab__header a.active").trigger("click");

    $(".aside_open").click(function() {
        $(this).hide();
        $(".editor__aside").parent().next().removeClass("col-12").addClass("col-10");
        $(".editor__aside").animate({
            "left" : "0"
        }, 100).parent().removeClass("d-none");

        $(".editor__aside").addClass("active");
    });

    $(".aside__close").click(function() {
        $(".editor__aside").animate({
            "left" : "-100%"
        }, 100).parent().addClass("d-none");
        $(".editor__aside").parent().next().removeClass("col-10").addClass("col-12");
        $(".editor__aside").removeClass("active");

        $(".aside_open").show();
    });

    $(".gallery .gallery-item").dblclick(function() {
        $(this).appendTo($(".full-gallery-items"));
    });

    var treeViewToggler = $(".caret");
    var i;

    $(treeViewToggler).each(function() {
        $(this).click(function() {
            $(this).parent().children(".nested").slideToggle("ease");
            $(this).toggleClass("active").toggleClass("caret-down");
        });
    });

    $("#searchCategory").keypress(function(event) {
        var key = event.which,
            searchingText = $(this).val().trim().toLowerCase() || false,
            searchTarget = $(this).attr("search-target") || false;

        if (key == 13) {
            $(".founded").removeClass("founded");

            if (searchTarget && searchingText) {
                var searchBlock = $(`${searchTarget}`) || false;

                hideTree(searchTarget);

                if (searchBlock) {
                    searchBlock.find("li").each(function() {
                        if ($(this).find(".caret").length) {
                            let foundedBlock = $(this),
                                caretBlock = $(this).find(".category__header").eq(0) || false;

                            if (caretBlock && caretBlock.text().trim().toLowerCase().indexOf(searchingText.toLowerCase()) !== -1) {
                                caretBlock.addClass("founded");
                                foundedBlock
                                    .parents(".nested").each(function() {
                                            $(this).slideDown("ease");

                                            $(this).siblings(".caret").each(function() {
                                                $(this)
                                                    .addClass("active")
                                                    .addClass("caret-down")
                                            })
                                    });
                            }
                        } else if ($(this).text().trim().toLowerCase().indexOf(searchingText.toLowerCase()) !== -1) {
                            foundedBlock = $(this);

                            foundedBlock
                                .addClass("founded")
                                .parents(".nested").each(function() {
                                        $(this).slideDown("ease");

                                        $(this).siblings(".caret").each(function() {
                                            $(this)
                                                .addClass("active")
                                                .addClass("caret-down")
                                        })
                                });
                        }
                    });
                }
            }

            $(`${searchTarget}`).animate({
                scrollTop: $(".founded").offset().top
            }, 2000);
        }
    });

    $("[hide-target]").click(function() {
        let hideTarget = $(this).attr("hide-target") || false;

        if (hideTarget) {
            hideTree(hideTarget);
        }
    });

    $("[expand-target]").click(function() {
        let expandTarget = $(this).attr("expand-target") || false;

        if (expandTarget) {
            expandTree(expandTarget);
        }
    });

    $("[category-name]").click(function() {
        $(this).toggleClass("checked");
    })

    $("#Tags .tag").click(function() {
        $(this).toggleClass("checked");
    });

    $("#searchTag").keypress(function(event) {
        var key = event.which,
            searchingText = $(this).val().trim().toLowerCase() || false,
            searchTarget = $(this).attr("search-target") || false;

        if (key == 13) {
            $(".founded").removeClass("founded");
            $(".tag").fadeIn();

            if (searchTarget && searchingText) {
                $(".founded").removeClass("founded");
                $(searchTarget)
                    .find(".tag")
                    .each(function() {
                        if ($(this).text().trim().toLowerCase().indexOf(searchingText.toLowerCase()) !== -1) {
                            $(this).addClass("founded");
                        }
                    });

                $(".tag").each(function() {
                    if (!$(this).hasClass('founded')) {
                        $(this).fadeOut("ease");
                    }
                })
            }
        }

    });

    $("[show-target]").click(function() {
        var showTarget = $(this).attr("show-target") || false;

        if (showTarget) {
            $(`${showTarget}.founded`).removeClass("founded");
            $(`${showTarget}`).fadeIn("ease");
        }
    });

    $("[clear-target]").click(function() {
        var clearTarget = $(this).attr("clear-target") || false,
            showTarget = $(this).attr("show-target") || false;

        if (clearTarget && showTarget) {
            $(`${clearTarget}`).val("");
        }
    })
});

function hideTree(hideTarget) {
    $(`${hideTarget}`).find(".caret").removeClass(["active", "caret-down"]);
    $(`${hideTarget}`).find(".nested").slideUp("ease");
}

function expandTree(expandTarget) {
    $(`${expandTarget}`).find(".caret").addClass(["active", "caret-down"]);
    $(`${expandTarget}`).find(".nested").slideDown("ease");
}
