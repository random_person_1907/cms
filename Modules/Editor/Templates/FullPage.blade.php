<form action="/modules?controllerName=AdminPageController&action=createPage" method="POST">
<div class="row justify-content-center">
    <div class="col-2 no-padding d-none">
        <div class="editor__aside">
            @include("Sidebar")
        </div>
    </div>
    <div class="col-12">
        <h1>{{ $Language->__("TEXT_DASHBOARD_CREATE_PAGE") }}</h1>
        <div class="tabs">
            <div class="tab__headers d-flex">
                <div class="tab__header">
                    <a tab-target="#Editor">
                        <span class="full-name">{{ $Language->__("TEXT_DASHBOARD_EDITOR") }}</span>
                    </a>
                </div>
                <div class="tab__header">
                    <a tab-target="#DescriptionAndPublishing">
                        <span class="full-name">{{ $Language->__("TEXT_DASHBOARD_DESCRIPTION_AND_PUBLISHING") }}</span>
                    </a>
                </div>
                <div class="tab__header">
                    <a class="active" tab-target="#SEO">
                        <span class="full-name">{{ $Language->__("TEXT_DASHBOARD_SEO") }}</span>
                    </a>
                </div>
                <div class="tab__header">
                    <a tab-target="#CategoriesTypeAndTags">
                        <span class="full-name">{{ $Language->__("TEXT_DASHBOARD_CATEGORIES_TYPE_AND_TAGS") }}</span>
                    </a>
                </div>
                <div class="tab__header">
                    <a tab-target="#Images">
                        <span class="full-name">{{ $Language->__("TEXT_DASHBOARD_IMAGES") }}</span>
                    </a>
                </div>
                <div class="tab__header">
                    <a tab-target="#Design">
                        <span class="full-name">{{ $Language->__("TEXT_DASHBOARD_DESIGN") }}</span>
                    </a>
                </div>
                <div class="tab__header">
                    <a tab-target="#Modules">
                        <span class="full-name">{{ $Language->__("TEXT_DASHBOARD_MODULES") }}</span>
                    </a>
                </div>
                <div class="tab__header">
                    <a tab-target="#Translates">
                        <span class="full-name">{{ $Language->__("TEXT_DASHBOARD_TRANSLATES") }}</span>
                    </a>
                </div>
                <div class="tab__header">
                    <a tab-target="#Access">
                        <span class="full-name">{{ $Language->__("TEXT_DASHBOARD_ACCESS") }}</span>
                    </a>
                </div>
            </div>
            <div class="tabs__container h-100">
                <div class="tab__content" id="Editor">
                  <div class="row justify-content-center">
                    <div class="col-4">
                      <input
                          type="text"
                          class="w-100 page-name block white input animation"
                          name="pageName"
                          placeholder="Enter page name here..."
                        >
                    </div>
                  </div>
                  <div class="row h-75 mt-5 justify-content-center">
                    <div class="col-8">
                      <div class="main-editor editor animation editor h-100 w-100">
                      </div>
                    </div>
                    <div class="col-2 mt-5">
                      <div class="shortcodes-module block white w-100 h-100">
                        <h2 class="header mt-3 pb-3">Shortcodes</h2>
                        <div class="shortcodes list p-3">
                            <div class="shortcode mb-3 mt-3 p-2">
                                <div class="shortcode__header">Comments</div>
                            </div>
                            <div class="shortcode mb-3 mt-3 p-2">
                                <div class="shortcode__header">Menu</div>
                            </div>
                            <div class="shortcode mb-3 mt-3 p-2">
                                <div class="shortcode__header">Pages</div>
                            </div>
                            <div class="shortcode mb-3 mt-3 p-2">
                                <div class="shortcode__header">Tags</div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab__content" id="DescriptionAndPublishing">
                    <div class="row h-75 mt-3 justify-content-center">
                        <div class="col-8">
                            <div class="block white no-border">
                                <h2 class="header no-border p-3 m-0">Description</h2>
                            </div>
                            <div class="additional-editor editor animation h-100 w-100">
                            </div>
                        </div>
                        <div class="col-3 mt-5">
                          <div class="shortcodes-module block white w-100 h-100">
                            <h2 class="header mt-3 pb-3">Additional information</h2>
                            <div class="aditional-information list p-3">
                                <div class="additional-informaion__element">
                                    <div class="d-flex flex-wrap justify-content-center mt-1 pb-3 pt-1">
                                        <label
                                            for="pageType"
                                            class="w-100 text-center mb-3 no-select"
                                        >Page type</label>
                                        <select
                                            id="pageType"
                                            class="w-50 page-name block white input animation select"
                                            name="pageType"
                                        >
                                            <option value="">Simple page</option>
                                            <option value="">Category list</option>
                                            <option value="">Shop</option>
                                        </select>
                                    </div>
                                    <div class="block d-flex justify-content-center mt-3 pb-3 pt-3 flex-wrap">
                                        <label for="publishedTime" class="w-100 text-center no-select">Published date</label>
                                        <input
                                            id="publishedTime"
                                            type="time"
                                            class="m-2 input block white"
                                            name="publishedTime"
                                          >
                                          <input
                                              id="publishedDate"
                                              type="date"
                                              class="m-2 input block white"
                                              name="publishedDate"
                                            >
                                    </div>
                                    <div class="block d-flex justify-content-center mt-3 pb-3 pt-3 flex-wrap">
                                        <label for="unpublishedTime" class="w-100 text-center no-select">Ubpublished date</label>
                                        <input
                                            id="unpublishedTime"
                                            type="time"
                                            class="m-2 input block white"
                                            name="unpublishedTime"
                                          >
                                          <input
                                              id="unpublishedDate"
                                              type="date"
                                              class="m-2 input block white"
                                              name="unpublishedDate"
                                            >
                                    </div>
                                    <div class="block d-flex justify-content-center mt-3 mb-3 pt-3">
                                        <input
                                            id="isPublished"
                                            type="checkbox"
                                            class="ml-3 d-none checkbox"
                                            name="isPublished"
                                          >
                                          <label for="isPublished" class="no-select">
                                              Published
                                              <span class="checkbox-box ml-3 animation">
                                              </span>
                                          </label>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="tab__content" id="SEO">
                    <div class="row h-85 mt-3 justify-content-center">
                        <div class="col-4 h-100">
                            <div class="block white h-100 w-100 m-3">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="block white no-border">
                                            <h2 class="header no-border p-3 m-0">Seo</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-10">
                                        <div class="block d-flex justify-content-center mt-3 pb-3 pt-3">
                                            <label for="pageTitle" class="mt-3 w-25 text-center no-select">Title</label>
                                            <input
                                                id="pageTitle"
                                                type="text"
                                                class="m-2 input block white animation w-75"
                                                name="pageTitle"
                                                placeholder="Enter page title here..."
                                              >
                                        </div>
                                        <div class="block d-flex justify-content-center mt-3 pb-3 pt-3">
                                            <label for="pageKeywords" class="mt-3 w-25 text-center no-select">Keywords</label>
                                            <input
                                                id="pageKeywords"
                                                type="text"
                                                class="m-2 input block white animation w-75"
                                                name="pageKeywords"
                                                placeholder="Enter page keywords here..."
                                              >
                                        </div>
                                        <div class="block d-flex justify-content-center mt-3 pb-3 pt-3">
                                            <label for="pageDescription" class="mt-3 w-25 text-center no-select">Description</label>
                                            <textarea
                                                id="pageDescription"
                                                type="text"
                                                class="m-2 input block white animation w-75"
                                                name="pageDescription"
                                                placeholder="Enter page description here..."
                                                cols="30"
                                                rows="5"
                                              ></textarea>
                                        </div>
                                        <div class="block d-flex justify-content-center mt-3 pb-3 pt-3">
                                            <label for="pageAlias" class="mt-3 w-25 text-center no-select">Alias</label>
                                            <input
                                                id="pageAlias"
                                                type="text"
                                                class="m-2 input block white animation w-75"
                                                name="pageAlias"
                                                placeholder="Enter page alias here..."
                                              >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab__content" id="CategoriesTypeAndTags">
                    <div class="row h-85 mt-3 justify-content-center">
                        <div class="col-4 h-100">
                            <div class="block white h-100 w-100 m-3 categories__block">
                                <h2 class="header mt-3 pb-3">Categories</h2>
                                <div class="w-100 d-flex justify-content-center">
                                    <input
                                        type="text"
                                        id="searchCategory"
                                        search-target="#Categories"
                                        class="w-75 m-3 input block white"
                                        placeholder="Input category name..."
                                    >
                                    <span
                                        class="control mt-4 text-center"
                                        show-target="[category-name]"
                                        clear-target="#searchCategory"
                                    >X</span>
                                </div>
                                <div id="Categories" class="tree-view pb-5 pt-0 scrollable h-65">
                                    <ul>
                                     <li>
                                        <span class="caret"></span>
                                        <span
                                            class="category__header"
                                            category-name="Beverages"
                                        >Beverages</span>
                                        <ul class="nested">
                                            <li category-name="Water">Water</li>
                                            <li category-name="Water">Water</li>
                                            <li category-name="Water">Water</li>
                                            <li category-name="Water">Water</li>
                                            <li category-name="Water">Water</li>
                                            <li category-name="Water">Water</li>
                                            <li category-name="Water">Water</li>
                                            <li category-name="Water">Water</li>
                                            <li category-name="Water">Water</li>
                                            <li category-name="Water">Water</li>
                                            <li category-name="Coffee">Coffee</li>
                                            <li>
                                                <span class="caret"></span>
                                                <span
                                                    class="category__header"
                                                    category-name="Tea"
                                                >Tea</span>
                                                <ul class="nested">
                                                    <li category-name="Black Tea">Black Tea</li>
                                                    <li category-name="White Tea">White Tea</li>
                                                    <li>
                                                        <span class="caret"></span>
                                                        <span
                                                            class="category__header"
                                                            category-name="Green Tea"
                                                        >Green Tea</span>
                                                        <ul class="nested">
                                                            <li category-name="Sencha">Sencha</li>
                                                            <li category-name="Gyokuro">Gyokuro</li>
                                                            <li category-name="Matcha">Matcha</li>
                                                            <li category-name="Pi Lo Chun">Pi Lo Chun</li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                       </ul>
                                     </li>
                                   </ul>
                                </div>
                                <div class="row w-100">
                                    <div class="col-6">
                                        <div class="w-100 ml-4 mt-3 d-flex justify-content-start">
                                            <span
                                                class="expand-tree control"
                                                expand-target="#Categories"
                                            >Expand tree</span>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="w-100 mt-3 d-flex justify-content-end">
                                            <span
                                                class="hide-tree control"
                                                hide-target="#Categories"
                                            >Hide tree</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 h-100">
                            <div class="block white h-100 mt-3 tags__block">
                                <h2 class="header mt-3 pb-3">Tags</h2>
                                <div class="w-100 d-flex justify-content-center">
                                    <input
                                        type="text"
                                        id="searchTag"
                                        search-target="#Tags"
                                        class="w-75 m-3 input block white"
                                        placeholder="Input tag name..."
                                    >
                                    <span
                                        class="control mt-4 text-center"
                                        show-target=".tag"
                                        clear-target="#searchTag"
                                    >X</span>
                                </div>
                                <div class="w-85 h-85 mt-4 scrollable d-flex justify-content-center" id="Tags">
                                    <div class="tags__column d-flex flex-wrap h-85 w-100 justify-content-center scrollable">
                                        <div class="tag checked">
                                            <label class="tag__name">
                                                Shops
                                                <input type="hidden" name="tags[]">
                                            </label>
                                        </div>
                                        <div class="tag">
                                            <label class="tag__name">
                                                Computers
                                                <input type="hidden" name="tags[]">
                                            </label>
                                        </div>
                                        <div class="tag">
                                            <label class="tag__name">
                                                Books
                                                <input type="hidden" name="tags[]">
                                            </label>
                                        </div>
                                        <div class="tag">
                                            <label class="tag__name">
                                                Flowers
                                                <input type="hidden" name="tags[]">
                                            </label>
                                        </div>
                                        <div class="tag">
                                            <label class="tag__name">
                                                Software
                                                <input type="hidden" name="tags[]">
                                            </label>
                                        </div>
                                        <div class="tag">
                                            <label class="tag__name">
                                                Alcohol
                                                <input type="hidden" name="tags[]">
                                            </label>
                                        </div>
                                        <div class="tag">
                                            <label class="tag__name">
                                                video
                                                <input type="hidden" name="tags[]">
                                            </label>
                                        </div>
                                        <div class="tag">
                                            <label class="tag__name">
                                                Music
                                                <input type="hidden" name="tags[]">
                                            </label>
                                        </div>
                                        <div class="tag">
                                            <label class="tag__name">
                                                Tables
                                                <input type="hidden" name="tags[]">
                                            </label>
                                        </div>
                                        <div class="tag">
                                            <label class="tag__name">
                                                Phones
                                                <input type="hidden" name="tags[]">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab__content" id="Images">
                  <div class="row h-75  mt-5">
                    <div class="col-6 d-flex justify-content-center h-100">
                      <div class="white-block h-100 w-75">
                        <div class="images-container">
                            <div class="gallery">
                                <div class="gallery-item">
                                    <div class="content">
                                        <img src="https://source.unsplash.com/random/?tech,care" alt="">
                                        <div class="gallery-item_description">
                                            <p>TEST</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,studied" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,substance" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,choose" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,past" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,lamp" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,yet" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,eight" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,crew" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,event" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,instrument" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,practical" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,pass" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,bigger" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,number" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,feature" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,line" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,railroad" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,pride" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,too" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,bottle" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,base" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,cell" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,bag" alt=""></div>
                                </div>
                                <div class="gallery-item">
                                    <div class="content"><img src="https://source.unsplash.com/random/?tech,card" alt=""></div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-6 d-flex flex-column">
                      <div class="white-block h-50 w-75">
                        <div class="full-gallery-items">

                        </div>
                      </div>
                      <div class="white-block h-50 w-75 mt-5">

                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab__content" id="Design">

                </div>
                <div class="tab__content" id="Modules">

                </div>
                <div class="tab__content" id="Translates">

                </div>
                <div class="tab__content" id="Access">

                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-1">
            <input class="editor__button button editor__button_create" type="submit" name="PageSave" value="{{ $Language->__("TEXT_DASHBOARD_SAVE") }}">
        </div>
        <div class="col-2">
            <input class="editor__button button editor__button_create" type="submit" name="PageSaveAndClose" value="{{ $Language->__("TEXT_DASHBOARD_SAVE_AND_CLOSE") }}">
        </div>
        <div class="col-2">
            <input class="editor__button button editor__button_save_and_create" type="submit" name="PageSaveAndCreate" value="{{ $Language->__("TEXT_DASHBOARD_SAVE_AND_CREATE") }}">
        </div>
        <div class="col-1">
            <input class="editor__button button editor__button_cancel" type="submit" name="PageCancel" value="{{ $Language->__("TEXT_DASHBOARD_CANCEL") }}">
        </div>
    </div>
</div>
</form>
