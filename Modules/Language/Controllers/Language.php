<?php
    namespace Modules;

    use \Core\Abstracts\Module;

    use Core\Application;
    use Symfony\Component\Dotenv\Dotenv;

    class Language extends Module {
        private const LOCALIZATION_PATH = "/Localization/";

        private $currentLanguage;
        private $languagePath;
        private $filepath;

        public function load()
        {
            $LanguageModel = new LanguageModel;
            $this->setSettings($LanguageModel->getSettings());
            $this->getCurrentLanguage();
            $this->loadLocalizationFiles();
        }

        public function getCurrentLanguage()
        {
            if (isset($_SESSION["LANGUAGE"]) && $_SESSION["LANGUAGE"]) {
                $this->currentLanguage = $_SESSION["LANGUAGE"];
            } else {
                $this->currentLanguage = $_ENV["DEFAULT_LANGUAGE"];
            }

            return $this->currentLanguage;
        }

        private function loadLocalizationFiles()
        {
            $filesPath = ROOT . self::LOCALIZATION_PATH . $this->currentLanguage . "/";

            if (file_exists($filesPath)) {
                $files = \Libs\Files::getFiles($filesPath);

                foreach ($files as $file) {
                    $languageFile = new Dotenv();
                    $languageFile->load(
                        $filesPath . $file
                    );
                }
            }
        }

        public function __($langVariable)
        {
            return isset($_ENV[$langVariable]) ? $_ENV[$langVariable] : "";
        }

        public function changeLang()
        {
            if (isset(Application::$_GET["language"])) {
                $_SESSION["LANGUAGE"] = Application::$_GET["language"];
            }

            Application::redirect(Application::getLastVisitedPage());
        }

        public function show($parameters)
        {
            $this->settings = $parameters["Module"]->getSettings();
            $viewParameters = [
                "Page"            => $parameters["Page"],
                "Module"          => $parameters["Module"],
                "currentLanguage" => $this->getCurrentLanguage(),
                "listLanguages"   => $this->getListLanguages(),
                "Template"        => isset($parameters["Template"]) ? $parameters["Template"] : "Minimized"
            ];

            $languageView = new LanguageView;
            return $languageView->render($viewParameters);
        }

        public function getListLanguages()
        {
            $languageSelector = Application::getApplicationName() . "_LANGUAGES";

            $languages = [];
            if (isset($this->settings[$languageSelector])) {
                foreach($this->settings[$languageSelector] as $language) {
                    if ($language !== $this->currentLanguage) {
                        $languages[] = $language;
                    }
                }
            }

            return $languages;
        }
    }
?>
