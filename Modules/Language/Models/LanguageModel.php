<?php
    namespace Modules;

    use \Core\Application;

    class LanguageModel {
        public function getSettings() {
            $module = Application::$entityManager
                        ->getRepository("\Entities\Module")
                        ->findOneBy([
                "name" => "Language"
            ]);

            return ($module) ? $module->getSettings() : [];
        }
    }
?>
