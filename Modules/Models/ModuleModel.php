<?php
    namespace Core\Models;

    use \Core\Application;

    class ModuleModel {
        public function findModuleByName($moduleName)
        {
            $module = Application::$entityManager
                ->getRepository("\Entities\Module")
                ->findOneBy([
                    "name" => $moduleName
                ]);

            if ($module && $module->getPublished()) {
                return $module;
            } else {
                return false;
            }
        }
    }
?>
