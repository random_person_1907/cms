<?php
    namespace Modules;

    use \Core\Application;
    use \Core\Abstracts\Module;

    class User extends Module {
        private static $repository;
        private static $entityManager;

        public static function setRepository($repositoryName)
        {
            self::$repository = self::$entityManager->getRepository($repositoryName);
        }

        public static function setEntityManager($entityManager)
        {
            self::$entityManager = $entityManager;
        }

        public static function getUsers($parameters)
        {
            return self::$repository->findBy($parameters);
        }

        public static function getUserById($id)
        {
            return self::$repository->find($id);
        }

        public static function getUsersByLogin($login)
        {
            return self::$repository->findBy([
                "login" => $login
            ]);
        }

        public static function getUserByLogin($login)
        {
            return self::$repository->findOneBy([
                "login" => $login
            ]);
        }

        public static function getUsersByEmail($email)
        {
            return self::$repository->findBy([
                "email" => $email
            ]);
        }

        public static function getUserByEmail($email)
        {
            return self::$repository->findOneBy([
                "email" => $email
            ]);
        }

        public static function checkPassword($password, $inputedPassword)
        {
            return password_verify($inputedPassword, $password);
        }

        public static function checkIpAddress($ipAddresses)
        {
            foreach($ipAddresses as $ipAddress) {
                if ($ipAddress->getIpAddress() === $_SERVER["REMOTE_ADDR"]) {
                    return true;
                }
            }
            return false;
        }

        public static function checkCode($code, $email)
        {
            $user = self::getUserByEmail($email);

            if ($user->getCode() == $code) {
                $user->setCode("");

                self::$entityManager->persist($user);
                self::$entityManager->flush();

                return true;
            } else {
                return false;
            }
        }

        public static function generateCode($length = 64)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $code = '';
            for ($i = 0; $i < $length; $i++) {
                $code .= $characters[rand(0, $charactersLength - 1)];
            }
            return $code;
        }
    }
?>
