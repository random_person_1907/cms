<?php
    namespace Modules;

    use \Core\Application;
    use \Core\Abstracts\Module;

    class Shortcode extends Module {
        const MODULE_SHORTCODE = "/(?P<moduleShortcode>\[\[ (?P<moduleName>[^.]*).(?P<moduleMethod>[^:]*)[\s]*[:]?[\s]*(?P<moduleParameters>([^\]]*))\]\])/";
        const MODULE_PARAMETERS_SHORTCODE = "/[\s]*[|]?[\s]*(?P<parameterName>[^=]*)[\s]*[=]?[\s]*([\"]+(?P<parameterValue>(?:[^\"\\\\]|\\\\.)*)[\"]{1}|[^$]*)[|]?[\s]*/";

        private $entityManager;
        private $noParse = [];
        private $parseInEnd = [];

        public function setEntityManager($entityManager)
        {
            $this->entityManager = $entityManager;
        }

        public function render($Page)
        {
            if (!isset($Page->html))
                return false;

            $modules = $this->getShortcodes($Page->html);

            foreach ($modules as $module) {
                if ($this->noParse($module) || $this->parseInEnd($module))
                    continue;

                $this->renderModule($module, $Page);
            }
        }

        public function renderModule($module, $Page)
        {
            $result = $this->getModule($module["moduleName"]);

            if (isset($result["controller"]) && is_object($result["controller"])) {
                $moduleObject = $result["controller"];
            } else {
                $moduleObject = false;
            }

            $module["moduleParameters"]["Module"] = isset($result["module"]) ? $result["module"] : false;
            $module["moduleParameters"]["Page"] = $Page;
            $module["moduleParameters"]["Language"] = Application::$Language;

            if (
                $moduleObject &&
                method_exists($moduleObject, $module["moduleMethod"])
            ) {
                $moduleHtml = $moduleObject->{$module["moduleMethod"]}($module["moduleParameters"]);
            } else {
                $moduleHtml = "";
            }

            $Page->html = str_replace($module["moduleShortcode"], $moduleHtml, $Page->html);
        }

        private function getShortcodes($html)
        {
            preg_match_all(self::MODULE_SHORTCODE, $html, $matches, PREG_SET_ORDER);

            $modules = [];
            foreach ($matches as $match) {
                $modules[] = [
                    "moduleShortcode" => trim($match["moduleShortcode"]),
                    "moduleName" => trim($match["moduleName"]),
                    "moduleMethod" => trim($match["moduleMethod"]),
                    "moduleParameters" => $this->parseParameters($match["moduleParameters"]),
                ];
            }

            return $modules;
        }

        private function noParse($module)
        {
            if (
                isset($module["moduleParameters"]["noParse"]) &&
                !$module["moduleParameters"]["noParse"]
            ) {
                $this->noParse[] = $module;
                return true;
            } else {
                return false;
            }
        }

        private function parseInEnd($module)
        {
            if (
                isset($module["moduleParameters"]["parseInEnd"]) &&
                !$module["moduleParameters"]["parseInEnd"]
            ) {
                $this->parseInEnd[] = $module;
                return true;
            } else {
                return false;
            }
        }

        public function getNotParseShortcodes()
        {
            return $this->noParse;
        }

        public function getParseInEndShortcodes()
        {
            return $this->parseInEnd;
        }

        private function parseParameters($parametersString)
        {
            if (!strlen(trim($parametersString)))
                return [];

            preg_match_all(self::MODULE_PARAMETERS_SHORTCODE, $parametersString, $matches, PREG_SET_ORDER);

            $parameters = [];
            foreach ($matches as $match) {
                if (isset($match["parameterName"]) && strlen(trim($match["parameterName"]))) {
                    $parameters[trim($match["parameterName"])] = isset($match["parameterValue"]) ? trim($match["parameterValue"]) : "";
                }
            }

            return $parameters;
        }

        private function getModule($name)
        {
            $module = $this
                ->entityManager
                ->getRepository("\Entities\Module")
                ->findOneBy([
                    "name" => $name
                ]);

            if ($module &&
                $module->getPublished()
            ) {
                $moduleFilepath = "return \"" . $module->getModuleFilepath() . "\";";
				$moduleFilepath = eval($moduleFilepath);

                if (file_exists(ROOT . $moduleFilepath . $module->getControllerFilepath())) {
    				require_once ROOT . $moduleFilepath . $module->getControllerFilepath();
                    require_once ROOT . $moduleFilepath . "Bootstrap.php";

                    $controllerName = "\Modules\\" . $module->getName();
                    $controller = new $controllerName;

                    return [
                        "controller" => $controller,
                        "module"     => $module
                    ];
                }
            }
        }
    }
?>
