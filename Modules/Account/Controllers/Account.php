<?php
    namespace Modules;

    use \Core\Abstracts\Module;

    class Account extends Module {
        public function showMinimized($parameters)
        {
            require_once __DIR__ . "/../Bootstrap.php";

            if (!isset($parameters["UserId"]) || !isset($parameters["UserClass"]))
                return false;

            if (!isset($parameters["UserClassNamespace"]))
                $parameters["UserClassNamespace"] = "\Entities\\";

            $AccountModel = new AccountModel;
            $User = $AccountModel->getUser($parameters);

            $parameters["User"] = $User;
            $parameters["ModuleFilepath"] = $_ENV["MODULE_FOLDER_PATH"] . $this->getFilepath();

            $AccountView = new AccountView;
            return $AccountView->render($parameters);
        }
    }
?>
