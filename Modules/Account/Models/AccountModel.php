<?php
    namespace Modules;

    class AccountModel {
        public function getUser($parameters) {
            $UserClassName = $parameters["UserClassNamespace"] . $parameters["UserClass"];
            User::setRepository($UserClassName);
            $User = User::getUserById($parameters["UserId"]);

            return $User;
        }
    }
?>
