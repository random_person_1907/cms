<?php
    namespace Modules;

    class AccountView extends \Core\View {
        public function render($parameters)
        {
            $this->connectTemplateEngine([
                ROOT . $parameters["Page"]->getTheme()->getFilepath() . "overrides/"
            ]);

            $parameters["Page"]->view->addJs($parameters["ModuleFilepath"] . "js/*");
            $parameters["Page"]->view->addCss($parameters["ModuleFilepath"] . "css/*");

            return $this
                ->getTemplateEngine()
                ->view()
                ->make($parameters["Template"], [
                    "User" => $parameters["User"],
                    "Page" => $parameters["Page"],
                    "Language" => $parameters["Language"]
                ])
                ->render();
        }
    }
?>
