<?php
    namespace Modules;

    use \Core\Abstracts\Module;

    class Snippets extends Module {
        public function include($parameters)
        {
            if (!isset($parameters["type"]))
                return false;

            switch (trim($parameters["type"])) {
                case 'js':
                    return $this->includeJs($parameters);
                    break;

                case 'css':
                    return $this->includeCss($parameters);
                    break;

                case 'html':
                    return $this->includeHtml($parameters);
                    break;

                case 'php':
                    return $this->includePhp($parameters);
                    break;

                default:
                    return false;
            }
        }

        private function includeJs($parameters)
        {
            return $parameters["Page"]->view->generateJs();
        }

        private function includeCss($parameters)
        {
            return $parameters["Page"]->view->generateCss();
        }
    }
?>
