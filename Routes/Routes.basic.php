<?php
    /**
     * Routes
     *
     * Static routes for main parts of CMS. It`s AUTO GENERATED file.
     * All additional routes add into file Routes.additional.php
     *
     */
    $this->register("/admin/*", "Admin");
    $this->register("/modules", "Module");
    $this->register("/crm/*", "CRM");
    $this->register("/api/*", "Api");
?>
