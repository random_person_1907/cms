<?php
    require_once __DIR__ . "/vendor/autoload.php";
    require_once __DIR__ . "/Core/Libs/TablePrefix/Bootstrap.php";

    use Doctrine\ORM\Tools\Setup;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\Dotenv\Dotenv;
    use Doctrine\ORM\Tools\Console\ConsoleRunner;

    $dotenv = new Dotenv();
    $dotenv->load(
        __DIR__ . '/Configuration/.env',
        __DIR__ . '/Configuration/.env.dev',
        __DIR__ . '/Configuration/.env.cache',
        __DIR__ . '/Configuration/.env.log',
        __DIR__ . '/Configuration/.env.mail',
        __DIR__ . '/Configuration/.env.database'
    );

    $entitiesPaths = [
        __DIR__ . "/Core/MappedSuperclasses",
        __DIR__ . $_ENV["ADMIN_FOLDER_PATH"] . $_ENV["ADMIN_ENTITIES_PATH"],
        __DIR__ . $_ENV["COMMON_FOLDER_PATH"] . $_ENV["COMMON_ENTITIES_PATH"],
        __DIR__ . $_ENV["MODULE_FOLDER_PATH"] . $_ENV["MODULE_ENTITIES_PATH"]
    ];

    // the connection configuration
    $dbParams = array(
        'driver'   => $_ENV["DB_DRIVER"],
        'user'     => $_ENV["DB_USER"],
        'password' => $_ENV["DB_PASSWORD"],
        "host"     => $_ENV["DB_HOST"],
        'dbname'   => $_ENV["DB_NAME"]
    );

    $config = Setup::createAnnotationMetadataConfiguration($entitiesPaths, $_ENV["DEVELOPER_MODE"]);

    // Table Prefix
    $evm = new \Doctrine\Common\EventManager;
    $tablePrefix = new \DoctrineExtensions\TablePrefix('prefix_');
    $evm->addEventListener(\Doctrine\ORM\Events::loadClassMetadata, $tablePrefix);

    $entityManager = EntityManager::create($dbParams, $config, $evm);

    return ConsoleRunner::createHelperSet($entityManager);
?>
