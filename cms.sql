-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Июл 25 2019 г., 00:08
-- Версия сервера: 5.7.26-0ubuntu0.19.04.1
-- Версия PHP: 7.2.19-0ubuntu0.19.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cms`
--

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_adminipaddress_adminuser`
--

CREATE TABLE `prefix_adminipaddress_adminuser` (
  `adminipaddress_id` int(11) NOT NULL,
  `adminuser_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `prefix_adminipaddress_adminuser`
--

INSERT INTO `prefix_adminipaddress_adminuser` (`adminipaddress_id`, `adminuser_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_admin_access_types`
--

CREATE TABLE `prefix_admin_access_types` (
  `id` int(11) NOT NULL,
  `permissions` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_admin_content`
--

CREATE TABLE `prefix_admin_content` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  `accessType_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `prefix_admin_content`
--

INSERT INTO `prefix_admin_content` (`id`, `name`, `content`, `created_on`, `modified_on`, `published`, `accessType_id`) VALUES
(1, 'Create page', '[[ Editor.include:type=\"AdminEditor\" ]]', '2019-07-23 00:00:00', '2019-07-23 00:00:00', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_admin_ip_addresses`
--

CREATE TABLE `prefix_admin_ip_addresses` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `prefix_admin_ip_addresses`
--

INSERT INTO `prefix_admin_ip_addresses` (`id`, `ip_address`) VALUES
(1, '127.0.0.1');

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_admin_pages`
--

CREATE TABLE `prefix_admin_pages` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `theme_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL,
  `accessType_id` int(11) DEFAULT NULL,
  `adminContent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `prefix_admin_pages`
--

INSERT INTO `prefix_admin_pages` (`id`, `type_id`, `parent_id`, `theme_id`, `title`, `content`, `name`, `alias`, `published`, `accessType_id`, `adminContent_id`) VALUES
(1, NULL, NULL, 1, 'Login', 'Login content', 'Login', 'login', 1, NULL, NULL),
(2, NULL, NULL, 1, 'Dashboard', 'Dashboard', 'Dashboard', 'main', 1, NULL, NULL),
(3, NULL, 4, 1, 'Create page', '[[ Editor.include:Template=\"FullPage\" ]]', 'Create page', 'create', 1, NULL, NULL),
(4, NULL, NULL, 1, 'Pages', '', 'Pages', 'pages', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_admin_page_types`
--

CREATE TABLE `prefix_admin_page_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_admin_themes`
--

CREATE TABLE `prefix_admin_themes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `filepath` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `prefix_admin_themes`
--

INSERT INTO `prefix_admin_themes` (`id`, `name`, `description`, `filepath`) VALUES
(1, 'Original', 'Original theme for admin panel', '/Admin/Templates/Original/');

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_admin_type`
--

CREATE TABLE `prefix_admin_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` varchar(10000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `prefix_admin_type`
--

INSERT INTO `prefix_admin_type` (`id`, `name`, `permissions`) VALUES
(1, 'Superuser', '777');

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_admin_user`
--

CREATE TABLE `prefix_admin_user` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `additionalEmail` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `isBanned` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telegram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsApp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `viber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedIn` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `prefix_admin_user`
--

INSERT INTO `prefix_admin_user` (`id`, `type_id`, `name`, `login`, `email`, `password`, `additionalEmail`, `code`, `avatar`, `isBanned`, `created_on`, `modified_on`, `skype`, `telegram`, `whatsApp`, `viber`, `linkedIn`, `phone`) VALUES
(1, 1, 'Random Person', 'random.person', 'random.person.1907@gmail.loc', '$2y$10$dM7rFOmB97NCCUznbLlnierxdQvTZcGHuikm3IoLkW2cblOyJI/dy', '', '', '/Upload/Admin/Avatars/randomPersonAvatar.jpg', 0, '2019-07-21 00:00:00', '2019-07-21 00:00:00', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_modules`
--

CREATE TABLE `prefix_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(3000) COLLATE utf8_unicode_ci NOT NULL,
  `moduleFilepath` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `controllerFilepath` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  `settings` varchar(10000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `prefix_modules`
--

INSERT INTO `prefix_modules` (`id`, `name`, `description`, `moduleFilepath`, `controllerFilepath`, `created_on`, `modified_on`, `published`, `settings`) VALUES
(1, 'Account', 'Account module', 'Account/', 'Controllers/Account.php', '2019-07-21 00:00:00', '2019-07-21 00:00:00', 1, ''),
(2, 'Snippets', 'Snippets module', 'Snippets/', 'Controllers/Snippets.php', '2019-07-21 00:00:00', '2019-07-21 00:00:00', 1, ''),
(3, 'Language', 'Language module', 'Language/', 'Controllers/Language.php', '2019-07-21 00:00:00', '2019-07-21 00:00:00', 1, '{\"ADMIN_DEFAULT_LANGUAGE\":\"English\",\"ADMIN_LANGUAGES\":[\"English\",\"Russian\"],\"COMMON_DEFAULT_LANGUAGE\":\"English\",\"COMMON_LANGUAGES\":[\"English\",\"Russian\"],\"CRM_DEFAULT_LANGUAGE\":\"English\",\"CRM_LANGUAGES\":[\"English\",\"Russian\"]}'),
(4, 'Editor', 'Editor module', 'Editor/', 'Controllers/Editor.php', '2019-07-21 00:00:00', '2019-07-21 00:00:00', 1, '');

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_shortcodes`
--

CREATE TABLE `prefix_shortcodes` (
  `id` int(11) NOT NULL,
  `content` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `prefix_adminipaddress_adminuser`
--
ALTER TABLE `prefix_adminipaddress_adminuser`
  ADD PRIMARY KEY (`adminipaddress_id`,`adminuser_id`),
  ADD KEY `IDX_2B8E6E07E596FEF1` (`adminipaddress_id`),
  ADD KEY `IDX_2B8E6E0739505EF` (`adminuser_id`);

--
-- Индексы таблицы `prefix_admin_access_types`
--
ALTER TABLE `prefix_admin_access_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prefix_admin_content`
--
ALTER TABLE `prefix_admin_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_72703078149C7BE` (`accessType_id`);

--
-- Индексы таблицы `prefix_admin_ip_addresses`
--
ALTER TABLE `prefix_admin_ip_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prefix_admin_pages`
--
ALTER TABLE `prefix_admin_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5A6D71AFC54C8C93` (`type_id`),
  ADD KEY `IDX_5A6D71AF149C7BE` (`accessType_id`),
  ADD KEY `IDX_5A6D71AF727ACA70` (`parent_id`),
  ADD KEY `IDX_5A6D71AF59027487` (`theme_id`),
  ADD KEY `IDX_5A6D71AF6F960412` (`adminContent_id`);

--
-- Индексы таблицы `prefix_admin_page_types`
--
ALTER TABLE `prefix_admin_page_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prefix_admin_themes`
--
ALTER TABLE `prefix_admin_themes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prefix_admin_type`
--
ALTER TABLE `prefix_admin_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prefix_admin_user`
--
ALTER TABLE `prefix_admin_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FE5DA4C2C54C8C93` (`type_id`);

--
-- Индексы таблицы `prefix_modules`
--
ALTER TABLE `prefix_modules`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prefix_shortcodes`
--
ALTER TABLE `prefix_shortcodes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `prefix_admin_access_types`
--
ALTER TABLE `prefix_admin_access_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `prefix_admin_content`
--
ALTER TABLE `prefix_admin_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `prefix_admin_ip_addresses`
--
ALTER TABLE `prefix_admin_ip_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `prefix_admin_pages`
--
ALTER TABLE `prefix_admin_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `prefix_admin_page_types`
--
ALTER TABLE `prefix_admin_page_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `prefix_admin_themes`
--
ALTER TABLE `prefix_admin_themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `prefix_admin_type`
--
ALTER TABLE `prefix_admin_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `prefix_admin_user`
--
ALTER TABLE `prefix_admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `prefix_modules`
--
ALTER TABLE `prefix_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `prefix_shortcodes`
--
ALTER TABLE `prefix_shortcodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `prefix_adminipaddress_adminuser`
--
ALTER TABLE `prefix_adminipaddress_adminuser`
  ADD CONSTRAINT `FK_2B8E6E0739505EF` FOREIGN KEY (`adminuser_id`) REFERENCES `prefix_admin_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2B8E6E07E596FEF1` FOREIGN KEY (`adminipaddress_id`) REFERENCES `prefix_admin_ip_addresses` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `prefix_admin_content`
--
ALTER TABLE `prefix_admin_content`
  ADD CONSTRAINT `FK_72703078149C7BE` FOREIGN KEY (`accessType_id`) REFERENCES `prefix_admin_access_types` (`id`);

--
-- Ограничения внешнего ключа таблицы `prefix_admin_pages`
--
ALTER TABLE `prefix_admin_pages`
  ADD CONSTRAINT `FK_5A6D71AF149C7BE` FOREIGN KEY (`accessType_id`) REFERENCES `prefix_admin_access_types` (`id`),
  ADD CONSTRAINT `FK_5A6D71AF59027487` FOREIGN KEY (`theme_id`) REFERENCES `prefix_admin_themes` (`id`),
  ADD CONSTRAINT `FK_5A6D71AF6F960412` FOREIGN KEY (`adminContent_id`) REFERENCES `prefix_admin_content` (`id`),
  ADD CONSTRAINT `FK_5A6D71AF727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `prefix_admin_pages` (`id`),
  ADD CONSTRAINT `FK_5A6D71AFC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `prefix_admin_page_types` (`id`);

--
-- Ограничения внешнего ключа таблицы `prefix_admin_user`
--
ALTER TABLE `prefix_admin_user`
  ADD CONSTRAINT `FK_FE5DA4C2C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `prefix_admin_type` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
