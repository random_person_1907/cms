<?php

    namespace Common\Entities;
    /**
    * @Entity
    * @Table(name="UserIpAddresses")
    */
    class UserIpAddress extends \MappedSuperclasses\IpAddress
    {
        /**
         * @ManyToMany(targetEntity="\Common\Entities\User", mappedBy="ipAddresses")
         */
        protected $users;

        /**
         * Add user.
         *
         * @param \MappedSuperclasses\User $user
         *
         * @return IpAddress
         */
        public function addUser(\Common\Entities\User $user)
        {
            $this->users[] = $user;

            return $this;
        }

        /**
         * Remove user.
         *
         * @param \MappedSuperclasses\User $user
         *
         * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
         */
        public function removeUser(\Common\Entities\User $user)
        {
            return $this->users->removeElement($user);
        }

        /**
         * Get users.
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getUsers()
        {
            return $this->users;
        }
    }
