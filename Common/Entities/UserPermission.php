<?php

    namespace Common\Entities;
    /**
    * @Entity
    * @Table(name="UserPermissions")
    */
    class UserPermission extends \MappedSuperclasses\Permission
    {
        /**
         * @ManyToMany(targetEntity="\Common\Entities\UserType", mappedBy="permissions")
         */
        protected $types;
    }
