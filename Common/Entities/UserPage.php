<?php

    namespace Common\Entities;
    /**
    * @Entity
    * @Table(name="UserPages")
    */
    class UserPage extends \MappedSuperclasses\Page
    {
        /**
        * @ManyToOne(targetEntity="\Common\Entities\UserTheme", inversedBy="theme")
        * @JoinColumn(name="UserThemeId")
        */
        protected $theme;
    }
