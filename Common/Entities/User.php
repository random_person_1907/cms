<?php

    namespace Common\Entities;
    /**
    * @Entity
    * @Table(name="Users")
    */
    class User extends \MappedSuperclasses\User
    {
        /**
         * @ManyToMany(targetEntity="\Common\Entities\UserIpAddress", inversedBy="users")
         * @JoinTable(name="UsersIpAddresses",
         *  joinColumns={@JoinColumn(name="userId", referencedColumnName="id")},
         *  inverseJoinColumns={@JoinColumn(name="ipAddressId", referencedColumnName="id")}
         * )
         */
       protected $ipAddresses;

       /**
        * @ManyToOne(targetEntity="\Common\Entities\UserType", inversedBy="users")
        * @JoinColumn(name="TypeId")
        */
       protected $type;
    }
