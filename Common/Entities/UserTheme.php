<?php

    namespace Common\Entities;
    /**
    * @Entity
    * @Table(name="UserThemes")
    */
    class UserTheme extends \MappedSuperclasses\Theme
    {
        /**
        * @OneToMany(targetEntity="\Common\Entities\UserPage", mappedBy="pages")
        */
        private $pages;
    }
