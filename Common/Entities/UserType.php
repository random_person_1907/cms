<?php

    namespace Common\Entities;
    /**
    * @Entity
    * @Table(name="UserTypes")
    */
    class UserType extends \MappedSuperclasses\UserType
    {
        /**
         * @ManyToMany(targetEntity="\Common\Entities\UserPermission", mappedBy="types")
         */
        protected $permissions;

        /**
         * @OneToMany(targetEntity="\Common\Entities\User", mappedBy="type")
         */
        protected $users;

        /**
         * Add user.
         *
         * @param \Entities\User $user
         *
         * @return Type
         */
        public function addUser(\Entities\User $user)
        {
            $this->users[] = $user;

            return $this;
        }

        /**
         * Remove user.
         *
         * @param \Entities\User $user
         *
         * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
         */
        public function removeUser(\Entities\User $user)
        {
            return $this->users->removeElement($user);
        }

        /**
         * Get users.
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getUsers()
        {
            return $this->users;
        }
    }
